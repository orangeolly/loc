<?php
require_once ('config.php');
class dataLayer {
	var $log;
	var $mysqli;
	function __construct() {
		$this->log = Logger::getLogger ( 'SQL' );
		
		$this->mysqli = new mysqli ( MYSQL_HOST, MYSQL_USER, MYSQL_PWD, DB_NAME );
		if ($this->mysqli->connect_errno) {
			$this->log->trace ( "Failed to connect to MySQL: " . $this->mysqli->connect_error );
			die ( "Unable to connect to the database server at this time Host is:" . MYSQL_HOST . " User Is:" . MYSQL_USER . " Pwd is hidden " . MYSQL_PWD . " Error msg is:" . $this->mysqli->connect_error );
		} else {
			$this->log->trace ( "connection good" );
		}
	}
	function doQuery($queryString) {
		$this->log->trace ( $queryString );
		if ($this->mysqli->ping ()) {
			$this->log->trace ( "Our connection is ok!\n" );
		} else {
			$this->log->error ( "Error: %s\n", $this->mysqli->error );
		}
		
		if ($result = $this->mysqli->query ( $queryString )) {
			$this->log->trace ( "Select returned  rows." . $result->num_rows );
			return $result;
		} else {
			$this->log->error ( " Error with query $queryString Error was:" . $this->mysqli->error );
			return false;
		}
	}
	function doGetRowsSql($queryString) {
		$results = null;
		if ($result = $this->doQuery ( $queryString )) {
			$i = 0;
			while ( $row = $result->fetch_array ( MYSQLI_ASSOC ) ) {
				$results [$i ++] = $this->replaceSpacesWith__ ( $row );
			}
		} else {
			return $result;
		}
		return $results;
	}
	function doGetASingleRow($queryString) {
		if ($result = $this->doQuery ( $queryString )) {
			if ($row = $result->fetch_array ( MYSQLI_ASSOC )) {
				return $row;
			}
		} else {
			return $result;
		}
		return null; // "no rows returned";
	}

	function getGeneratedKey() {
		$idSQL = 'SELECT LAST_INSERT_ID();';
		if ($result = $this->mysqli->query ( $idSQL )) {
			if ($row = $result->fetch_array ( MYSQLI_ASSOC )) {
				$int = $row ['LAST_INSERT_ID()'];
				$this->log->trace ( "<br> new int key was " . $int . " " );
			}
			return $int;
		} else {
			$this->log->error ( 'Invalid query:$idSQL' . $this->mysqli->error );
			return $result;
		}
	}
	
	/**
	 * ********** This next section is the code for generic access to tables with one or no key *
	 */
	function makeAndRefreshItem($dataObject, $specifyKey, $dontUpdateOnCopy) {
		$this->log->trace ( "In makeAndRefreshItem" );
		$schemaName = $dataObject ['tableName'];
		if ($schemaName == null) {
			$this->log->error ( 'Error Request to add an object that is missing a table name specifier from makeAndRefreshItem(...)' );
			return - 1;
		}
		$queryString = "################# makeAndRefreshItem($dataObject,$specifyKey, $dontUpdateOnCopy) #############
SELECT COLUMN_NAME, COLUMN_KEY, DATA_TYPE  FROM INFORMATION_SCHEMA.COLUMNS  WHERE TABLE_SCHEMA = '" .DB_NAME. "' AND table_name = '" . $schemaName . "' ;";
		$queryString2 = "INSERT INTO `" . $schemaName . "` (";
		$queryString2part2 = " VALUES ( ";
		if ($dontUpdateOnCopy) {
			$onCopySql = "";
			$queryString2 = "#################### makeAndRefreshItem($dataObject,$specifyKey, $dontUpdateOnCopy) sql2 ##################
INSERT IGNORE INTO `" . $schemaName . "` (";
		} else {
			$onCopySql = " ON DUPLICATE KEY UPDATE ";
		}
		$schemaDetail = array ();
		$intInsertId = - 1;
		$getReturnInt = false;
		if ($schemaDetail = $this->doQuery ( $queryString )) {
			while ( $column = $schemaDetail->fetch_array ( MYSQLI_ASSOC ) ) {
				$columnNameTableForm = $column ['COLUMN_NAME'];
				if ("lastModified" == $columnNameTableForm || "createdId" == $columnNameTableForm || "modifiedId" == $columnNameTableForm)
					continue;
				$columnName = str_replace ( " ", "__", $column ['COLUMN_NAME'] );
				if ($column ['COLUMN_KEY'] == "PRI" && $specifyKey == false) {
					if ($column ['DATA_TYPE'] == 'varchar' || $column ['DATA_TYPE'] == 'text') {
						$queryString2 .= '`' . $columnNameTableForm . "`, ";
						$UUID = uniqid ( '', true );
						$queryString2part2 .= " '" . $UUID . "', ";
					}
					if ($column ['DATA_TYPE'] == 'int' || $column ['DATA_TYPE'] == 'bigint') {
						$getReturnInt = true;
					}
				} else {
					if (isset ( $dataObject [$columnName] )) {
						$stringValueToInsert = "'" .$dataObject [$columnName]. "'";
					} else {
						$stringValueToInsert = 'DEFAULT';
					}
					$queryString2 .= '`' . $columnNameTableForm . "`, ";
					if ($column ['DATA_TYPE'] == 'varchar' || $column ['DATA_TYPE'] == 'text' || $column ['DATA_TYPE'] == 'enum' || $column ['DATA_TYPE'] == 'date' || $column ['DATA_TYPE'] == 'datetime' || $column ['DATA_TYPE'] == 'timestamp') {
						$queryString2part2 .= "$stringValueToInsert, ";
						$onCopySql .= "`$columnNameTableForm`=$stringValueToInsert, ";
					} else {
						$valueToInsert = $dataObject [$columnName];
						if ($dataObject [$columnName] == null) {
							$valueToInsert = 'DEFAULT';
						}
						$queryString2part2 .= $valueToInsert . ", ";
						$onCopySql .= '`' . $columnNameTableForm . "`=" . $valueToInsert . ", ";
					}
				}
			}
		}
		if ($dontUpdateOnCopy) {
			$onCopySql = "";
		}
		
		$queryString2 = rtrim ( $queryString2, ', ' );
		$onCopySql = rtrim ( $onCopySql, ', ' );
		$queryString2part2 = rtrim ( $queryString2part2, ', ' );
		$generatedSQL = $queryString2 . ")" . $queryString2part2 . ") " . $onCopySql . " ;";
		$result = $this->doQuery ( $generatedSQL );
		if (! $result) {
			return - 1;
		}
		if ($getReturnInt) {
			return $this->getGeneratedKey ();
		}
		return $UUID;
	}
	function findSchemaColumns($schemaName) {
		return $this->findSchemaColumnsDB ( $schemaName, null );
	}
	function findSchemaColumnsDB($schemaName, $database) {
		$results = array ();
		if ($database == null) {
			$database = DB_NAME;
		}
		$queryString = "############ findSchemaColumnsDB($schemaName,$database) ##################
SELECT COLUMN_NAME, COLUMN_KEY, DATA_TYPE  FROM INFORMATION_SCHEMA.COLUMNS  WHERE TABLE_SCHEMA = '" . $database . "' AND table_name = '" . $schemaName . "' ;";
		if ($result = $this->doQuery ( $queryString )) {
			$i = 0;
			while ( $row = $result->fetch_array ( MYSQLI_ASSOC ) ) {
				$row ['COLUMN_NAME'] = str_replace ( " ", "__", $row ['COLUMN_NAME'] );
				$results [$i ++] = $row;
			}
		}
		return $results;
	}
	function findSchemaKeyName($schemaName, $database) {
		if ($database == null) {
			$database = DB_NAME;
		}
		$queryString = "############# findSchemaKeyName($schemaName,$database) ###############
SELECT COLUMN_NAME, COLUMN_KEY, DATA_TYPE  FROM INFORMATION_SCHEMA.COLUMNS  WHERE TABLE_SCHEMA = '" . $database . "' AND table_name = '" . $schemaName . "' ;";
		$schemaDetail = array ();
		if ($schemaDetail = $this->doQuery ( $queryString )) {
			while ( $column = $schemaDetail->fetch_array ( MYSQLI_ASSOC ) ) {
				if ($column ['COLUMN_KEY'] == "PRI") {
					$idName = $column ['COLUMN_NAME'];
					return $idName;
				}
			}
		}
		$this->log->error ( "<br> no key found in findSchemaKeyName(...) for tableName " . $schemaName );
		return null;
	}
	function findItemWithId($schemaName, $idName, $idValue) {
		return $this->findItemWithIdDB ( $schemaName, $idName, $idValue, null );
	}
	function findItemWithIdDB($schemaName, $idName, $idValue, $database) {
		$this->log->trace ( "in findItemWithIdDB()  for table $schemaName with PK $idName the key value should be :$idValue in database $database" );
		
		if ($database == null) {
			$database = DB_NAME;
		}
		$queryString = "########findItemWithIdDB($schemaName,$idName,$idValue,$database)#########
SELECT COLUMN_NAME, COLUMN_KEY, DATA_TYPE  FROM INFORMATION_SCHEMA.COLUMNS  WHERE TABLE_SCHEMA = '" . $database . "' AND table_name = '" . $schemaName . "' ;";
		
		$queryString2 = "########sql 2 is from findItemWithIdDB($schemaName,$idName,$idValue,$database)########
SELECT $database.$schemaName.* FROM " . $database . "." . $schemaName . " WHERE ";
		$schemaDetail = array ();
		if ($schemaDetail = $this->doQuery ( $queryString )) {
			while ( $column = $schemaDetail->fetch_array ( MYSQLI_ASSOC ) ) {
				$columnName = str_replace ( " ", "__", $column ['COLUMN_NAME'] );
				$columnNameTableForm = $column ['COLUMN_NAME'];
				if ($columnName == $idName) {
					if ($column ['DATA_TYPE'] == 'varchar' || $column ['DATA_TYPE'] == 'text') {
						$comma = "'";
					} else {
						$comma = "";
					}
					$queryString2 .= "$database.$schemaName.`" . $columnNameTableForm . "` = " . $comma . $idValue . $comma . " ;";
				}
			}
		}
		$results = array ();
		if ($result = $this->doQuery ( $queryString2 )) {
			$i = 0;
			if ($row = $result->fetch_array ( MYSQLI_ASSOC )) {
				$row ['tableName'] = $schemaName;
				return $this->replaceSpacesWith__ ( $row );
			} else {
				return - 1;
			}
		}
		return - 1;
	}
	function findItem($schemaName, $idValue) {
		return $this->findItemDB ( $schemaName, $idValue, null );
	}
	function findItemDB($schemaName, $idValue, $database) {
		$this->log->trace ( "in findItemDB() finding primary key name for table $schemaName the key value should be :$idValue in database $database" );
		if ($database == null) {
			$database = DB_NAME;
		}
		$queryString = "#########findItemDB($schemaName,$idValue,$database)#############
SELECT COLUMN_NAME, COLUMN_KEY, DATA_TYPE  FROM INFORMATION_SCHEMA.COLUMNS  WHERE TABLE_SCHEMA = '" . $database . "' AND table_name = '" . $schemaName . "' ;";
		$schemaDetail = array ();
		if ($schemaDetail = $this->doQuery ( $queryString )) {
			while ( $column = $schemaDetail->fetch_array ( MYSQLI_ASSOC ) ) {
				$columnName = str_replace ( " ", "__", $column ['COLUMN_NAME'] );
				if ($column ['COLUMN_KEY'] == "PRI") {
					$idName = $columnName;
				}
			}
		}
		return $this->findItemWithIdDB ( $schemaName, $idName, $idValue, $database ); // this assumes keyname has __ rather than spaces
	}
	function removeItemDBEntry($object) {
		$queryString = "############ removeItemDBEntry($object) #############
SELECT COLUMN_NAME, COLUMN_KEY, DATA_TYPE  FROM INFORMATION_SCHEMA.COLUMNS  WHERE TABLE_SCHEMA = '" . DB_NAME . "' AND table_name = '" . $object['tableName'] . "' ;";
		$queryString2 = "DELETE FROM " . $object ['tableName'] . " WHERE ";
		$schemaDetail = array ();
		if ($schemaDetail = $this->doQuery ( $queryString )) {
			while ( $column = $schemaDetail->fetch_array ( MYSQLI_ASSOC ) ) {
				$columnNameTableForm = $column ['COLUMN_NAME'];
				$columnName = str_replace ( " ", "__", $column ['COLUMN_NAME'] ); // this version has spaces replaced by __
				if ($column ['COLUMN_KEY'] == "PRI") {
					$queryString2 .= $columnNameTableForm . " = ";
					if ($column ['DATA_TYPE'] == 'varchar' || $column ['DATA_TYPE'] == 'text') {
						$queryString2 .= " '" . $object [$columnName] . "';";
					} else {
						$queryString2 .= $object [$columnName] . ";";
					}
				}
			}
		}
		return $this->doQuery ( $queryString2 );
	}
	function findItems($schemaName) {
		return $this->findItemsWithFilter ( $schemaName, null, null, null );
	}
	function findItemsWithFilter($schemaName, $filter, $orderColumn, $desc) {
		$queryString = "##########findItemsWithFilter($schemaName, $filter,$orderColumn,$desc))#############
SELECT $schemaName.* FROM " . $schemaName . "  ";
		if (" " != $filter && "" != $filter && null != $filter) {
			$queryString .= " WHERE " . $filter;
		}
		if (" " != $orderColumn && "" != $orderColumn && null != $orderColumn) {
			$queryString .= " ORDER BY `" . $orderColumn . "` ";
			if ((isset ( $desc )) && ($desc == true)) {
				$queryString .= " DESC ";
			}
		}
		$queryString .= " ;";
		if ($result = $this->doQuery ( $queryString )) {
			$i = 0;
			while ( $row = $result->fetch_array ( MYSQLI_ASSOC ) ) {
				$results [$i ++] = $this->replaceSpacesWith__ ( $row );
			}
		}
		return $results;
	}
	function replaceSpacesWith__($row) {
		foreach ( $row as $id => $value ) {
			$spaceFreeKey = str_replace ( " ", "__", $id );
			$noSpaceRow [$spaceFreeKey] = $value;
		}
		return $noSpaceRow;
	}
	function getUserData($username) {
		$queryString = "############### getUserData($username) ################
		SELECT * FROM registereduser WHERE username = '$username' ";
		return $this->doGetASingleRow ( $queryString );
	}
	function updateLoginAttemptTime($username, $time, $delay) {
		$queryString = "############### updateLoginAttemptTime($useName, $time, $delay) ################
		update registereduser set lastLoginAttempt=$time, delay=$delay WHERE username ='$username'";
		return $this->doQuery ( $queryString );
	}
	function getAllVacancyData() {
		$queryString = "############### getAllVacancyData() ################
		SELECT * FROM jobvacancies as jv
		INNER JOIN registereduser as ru on jv.vacancyAuthor = ru.userId
		LEFT OUTER JOIN practice as pr on jv.vacancyAuthor = pr.userId
		WHERE ru.approval = 'approved';
		";
		return $this->doGetRowsSql ( $queryString );
	}
	function getAllEmployeeData() {
		$queryString = "############### getAllEmployeeData() ################
		SELECT * FROM employee as em
		INNER JOIN registereduser as ru on em.userId = ru.userId
		WHERE ru.approval = 'approved';			
		";
		return $this->doGetRowsSql ( $queryString );
	}
	function getNews() {
		$queryString = "############### getNews() ################
		SELECT  n.*, ru.fullName FROM news as n
		INNER JOIN registereduser as ru on n.newsAuthor = ru.userId
		WHERE newsStatus != 'archived' 
		AND ru.approval = 'approved'
		ORDER BY `newsId` DESC;			
		";
		return $this->doGetRowsSql ( $queryString );
	}

	function getCourses() {
		$queryString = "############### getCourses() ################
		SELECT * FROM course as c
		INNER JOIN registereduser as ru on c.courseAuthor = ru.userId
		WHERE ru.approval = 'approved'
		 ORDER BY `courseName`;
		";
		return $this->doGetRowsSql ( $queryString );
	}
	function getEmployeeData($userId) {
		$queryString = "############### getEmployeeData($userId) ################
		SELECT * FROM employee as em
		INNER JOIN registereduser as ru on em.userId = ru.userId
		WHERE em.userId = '$userId'
		AND ru.approval = 'approved';";
		return $this->doGetASingleRow ( $queryString );
	}
	function getPracticeData($userId) {
		$queryString = "############### getPracticeData($userId) ################
		SELECT * FROM practice as pr
		INNER JOIN registereduser as ru on pr.userId = ru.userId
		WHERE pr.userId = '$userId'
		AND ru.approval = 'approved';";
		return $this->doGetASingleRow ( $queryString );
	}
	function getAllPracticeData() {
		$queryString = "############### getAllPracticeData() ################
		SELECT * FROM practice as pr
		INNER JOIN registereduser as ru on pr.userId = ru.userId
		WHERE ru.approval = 'approved';
		";
		return $this->doGetRowsSql ( $queryString );
	}
	function getCommitteeMembers() {
		$queryString = "############### getCommitteeMembers() ################
		SELECT * FROM registereduser 
		WHERE member='Yes' & rank IS NOT NULL
		AND approval = 'approved'
		ORDER BY rank;	
		";
		return $this->doGetRowsSql ( $queryString );
	}
	function approveApplicant($userId) {
		$queryString = "############### approveApplicant($userId) ################
		update registereduser set approval='approved'  WHERE userId=$userId;";
		return $this->doQuery ( $queryString );
	}
	function removeUser($userId) {
		//$queryString = "############### removeUser from registered user($userId) ################
		//DELETE from registereduser WHERE userId=$userId;";
		$queryString = "############### removeUser from registered user($userId) ################
		Update registereduser set approval='rejected' WHERE userId=$userId;";
		$this->doQuery ( $queryString );
		$queryString = "############### removeUser from vacancies($userId) ################
		DELETE from vacancies WHERE userId=$userId;";
		$this->doQuery ( $queryString );
		$queryString = "############### removeUser from practice Info($userId) ################
		DELETE from practice WHERE userId=$userId;";
		$this->doQuery ( $queryString );
	}
	function deleteOldVacancies() {
		$queryString = "############### deleteOldVacancies() ################
		DELETE FROM jobvacancies WHERE TIMESTAMPADD(MONTH,duration,postedDate) < CURRENT_TIMESTAMP;";
		return $this->doQuery ( $queryString );
	}
	function subscribeNews($userId, $value) {
		$queryString = "############### subscribeNews() ################
		update registereduser set subscribedToNews=$value  WHERE userId=$userId
		AND  approval = 'approved'";
		return $this->doQuery ( $queryString );
	}
	function subscribeCourses($userId, $value) {
		$queryString = "############### subscribeCourses() ################
		update registereduser set subscribedToCourses=$value  WHERE userId=$userId
		AND  approval = 'approved'
		;";
		return $this->doQuery ( $queryString );
	}
	
	function updatePasswordForUsername($username,$hash)
	{
		$queryString = "############### updatePasswordForUsername() ################
		update registereduser set hash='$hash'  WHERE username='$username';";
		return $this->doQuery ( $queryString );	
	}
}

