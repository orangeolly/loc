<?php
if (! session_start ()) {
	echo ("<br><br><br>session has not started");
}
// echo("started<br>");
require_once ('config.php');
if (DEBUG)
	echo ("doneConfig<br>");
require_once ('view.php');
if (DEBUG)
	echo ("included headers<br>");
class avonloc {
	var $view;
	var $memberAccess;
	var $firstReg = true;
	
	// Constructor
	function __construct() {
		if (DEBUG)
			echo ("in avonloc constructor about to create the view logger <br>");
		$this->log = Logger::getLogger ( 'avonLocView' );
		$this->log->debug ( " " );
		$this->log->debug ( " --------------------- Start of a vistit ----------------------------------" );
		$this->view = new view ();
		$this->log->trace ( " done avonLocMain() constructor" );
	}
                    function followInstruction($instruction) {
                        $this->log->info ( "Command was " . $instruction . " done" );
                        $this->log->trace ( "Allow was " . $this->memberAccess );
                        // echo ("command being handled is :".$instruction.$this->memberAccess);
                        if ($this->memberAccess == false) {
                            switch ($instruction) {
                                case "home" :
                                    $this->view->DisplayHomePage ();
                                    break;
                                case "login" :
                                    $this->view->Login ( false );
                                    break;
                                case "loginAgain" :
                                    $this->view->Login ( true );
                                    break;

                                case "tryLogin" :
                                    $this->view->TryLogin ();
                                    break;
                                case "viewPractices" :
                                    $this->view->ViewPractices ();
                                    break;

                                case "about" :
                                    $this->view->About ();
                                    break;

                                case "lostPassword" :
                                    $this->view->LostPassword();
                                    break;


                                case "employees" :
                                    $this->view->Employees ();
                                    break;

                                case "viewDownloads" :
                                    $this->view->ViewDownloads ("Useful documents for AvonLOC members","Download");
                                    break;

                                case "viewBanes" :
                                    $this->view->ViewDownloads ("Bath and North East Somerset documents","Banes");
                                    break;

                                case "viewGuidelines" :
                                    $this->view->ViewDownloads ("Referral Guidelines","Guideline");
                                    break;

                                case "viewLOC" :
                                    $this->view->ViewDownloads ("LOC documents","LOC");
                                    break;

                                case "viewEnhancedServices" :
                                    $this->view->ViewDownloads ("Enhanced services documents","Enhanced services");
                                    break;

                                case "viewLinks" :
                                    $this->view->ViewLinks ();
                                    break;

                                case "contact" :
                                    $this->view->Contact ();
                                    break;

                                case "viewCourses" :
                                    $this->view->ViewCourses ();
                                    break;

                                case "readNews" :
                                    $this->view->ReadNews ( 0 );
                                    break;

                                case "subscribe" :
                                    $this->view->Subscribe ();
                                    break;
                                default :
                                    $this->log->debug("command now recognised: ".$instruction);
                                    $this->view->DisplayHomePage ();
                            }
                        } else {
                            switch ($instruction) {
                                case "home" :
                                    $this->view->DisplayHomePage ();
                                    break;

                                case "login" :
                                    $this->view->Login ( false );
                                    break;
                                case "register" :
                                    $this->view->Register ( $this->firstReg );
                                    break;

                                case "regSuccess" :
                                    $this->view->ShowRegSuccess ();
                                    break;

                                case "vacancies" :
                                    $this->view->Vacancies ();
                                    break;
                                case "employees" :
                                    $this->view->Employees ();
                                    break;
                                case "postVacancies" :
                                    $this->view->PostVacancies ();
                                    break;
                                case "submitVacancy" :
                                    $this->view->SubmitVacancies ();
                                    break;
                                case "offerServices" :
                                    $this->view->OfferServices ();
                                    break;
                                case "submitServices" :
                                    $this->view->SubmitServices ();
                                    break;
                                case "practiceInfo" :
                                    $this->view->PracticeInfo ();
                                    break;
                                case "viewPractices" :
                                    $this->view->ViewPractices ();
                                    break;
                                case "maintainPractice" :
                                    $this->view->MaintainPractice ();
                                    break;
                                case "submitPractice" :
                                    $this->view->SubmitPractice ();
                                    break;

                                case "committeePics" :
                                    $this->view->UploadPics ();
                                    break;

                                case "submitCommitteePic" :
                                    $this->view->SubmitCommitteePics ();
                                    break;

                                case "about" :
                                    $this->view->About ();
                                    break;

                                case "postNews" :
                                    $this->view->PostNews ();
                                    break;

                                case "readNews" :
                                    $this->view->ReadNews ( 0 );
                                    break;
                                case "subscribeNews" :
                                    $this->view->SubscribeNews ();
                                    break;
                                case "unsubscribeNews" :
                                    $this->view->UnsubscribeNews ();
                                    break;

                                case "submitNews" :
                                    $this->view->SubmitNews ();
                                    break;
                                case "addCourse" :
                                    $this->view->AddCourse ();
                                    break;

                                case "viewCourses" :
                                    $this->view->ViewCourses ();
                                    break;

                                case "submitCourse" :
                                    $this->view->SubmitCourse ();
                                    break;
				case "subscribeCourses" :
					$this->view->SubscribeCourses ();
					break;
				case "unsubscribeCourses" :
					$this->view->UnsubscribeCourses ();
					break;
				
				case "submitDownload" :
					$this->view->SubmitDownload ();
					break;
				
				case "manageDownloads" :
					$this->view->ManageDownloads ();
					break;
				
				case "deleteDownload" :
					$this->view->DeleteDownload ();
					break;

				case "viewDownloads" :
						$this->view->ViewDownloads ("Useful documents for AvonLOC members","Download");
						break;		
						
				case "viewBanes" :
					$this->view->ViewDownloads ("Bath and North East Somerset documents","Banes");
					break;
				
				case "viewGuidelines" :
					$this->view->ViewDownloads ("Referral Guidelines","Guideline");
					break;
				
				case "viewLOC" :
					$this->view->ViewDownloads ("LOC documents","LOC");
					break;
				
				case "viewEnhancedServices" :
					$this->view->ViewDownloads ("Enhanced services documents","Enhanced services");
					break;
				
				case "submitLink" :
					$this->view->SubmitLink ();
					break;
				
				case "manageLinks" :
					$this->view->ManageLinks ();
					break;
				
				case "viewLinks" :
					$this->view->ViewLinks ();
					break;
				
				case "deleteLink" :
					$this->view->DeleteLink ();
					break;
				
				case "manageApplications" :
					$this->view->MaintainApplicants ();
					break;
					
				case "mailingList":
				    $this->view->MailingList();
				    break;
				
				case "submitApplicant" :
					$this->view->SubmitApplicant ();
					break;
				case "deleteCourse" :
					$this->view->DeleteCourse ();
					break;
				case "deleteVacancy" :
					$this->view->DeleteVacancy ();
					break;
				case "deleteEmployee" :
					$this->view->DeleteEmployee ();
					break;
				case "archiveNews" :
					$this->view->ArchiveNews ();
					break;
				
				case "contact" :
					$this->view->Contact ();
					break;
				
				default :
					$this->view->DisplayHomePage ();
			}
		}
		$this->log->trace ( " done followInstruction($instruction) constructor" );
	}
	function doView() {
		if (DEBUG)
			echo ("in do view ");
		$this->view->showVariables ( DEBUG );
		$instruction = $_REQUEST ['command'];
		$this->memberAccess = false;
		// / has not logged in yet.
		if (! isset ( $_SESSION ['username'] )) {
			$this->log->trace ( " not logged in yet " );
			$this->memberAccess = false;
			$this->view->Logout ();
		} else { // / has logged in
			if (time () - $_SESSION ['login_time'] > 3000) // / timed out.
{
				$this->log->trace ( " the session has timed out so will log out " );
				$this->memberAccess = false;
				$this->view->Logout ();
			} else { // / is within the time since last visit and can stay logged in
				$this->log->trace ( " logged in and within time out period " );
				$_SESSION ['login_time'] = time (); // / reset the last visit time.
				if (DEBUG)
					echo ("<br>this session is " . var_dump ( $_SESSION ['username'] ));
				$this->memberAccess = true;
			}
		}
		
		// / trying to register
		if ($instruction == "submitRegistrationRequest") {
		    if (!$this->memberAccess)
            {
                $this->log->debug ( " Non member attempted to register, hack attempt probably " );
                $instruction="home";
            }
            else {// not a hack attempt
                $this->log->trace(" trying to reg ");
                $regSuccess = $this->view->SubmitRegistrationRequest();
                if ($regSuccess) {
                    $instruction = "regSuccess";
                    $this->log->trace(" going to inform user of need to wait");
                    // / Should follow through to trylogin and succeed now.
                } else {
                    $this->log->trace(" failed to register, going to try not for the first time (probably dup username) ");
                    // $this->view->Register(false);
                    $instruction = "register";
                    $this->firstReg = false;
                }
            }
		}
		// / actively trying to login now.
		$this->log->trace ( " memberAccess is now :" . $this->memberAccess );
		if ($instruction == "tryLogin") {
			$this->log->trace ( " trying to login " );
			$success = $this->view->TryLogin ();
			if ($success) {
				$instruction = "home";
				$this->memberAccess = true;
				$this->log->trace ( " login succeeded " );
			} else {
				$instruction = "loginAgain";
				$this->memberAccess = false;
				$this->log->trace ( " login failed " );
			}
		}
		
		if ($instruction == "logout") {
			$this->view->Logout ();
			$instruction = "home";
			$this->memberAccess = false;
		}
		$this->log->trace ( " Main thinks memberAccess is " . $this->memberAccess );
		$this->view->Header ( $this->memberAccess );
		$this->view->Menu ();
		$this->followInstruction ( $instruction );
		$this->view->Footer ();
		$_SESSION ["last"] = "a last value ";
		$this->log->trace ( " just set last at end of footer last:" . $_SESSION ['last'] . " test:" . $_SESSION ['test'] );
	}
}
if (DEBUG)
	echo ("just set test at beg of main ");
Logger::configure ( LOCAL_CONFIG_PATH . 'log4php_config.xml' );
if (DEBUG)
	echo "configured logger";
$avonloc = new avonloc ();
if (DEBUG)
	echo "created Class";
$avonloc->doView ();

?>