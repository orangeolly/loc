<script src="javascript/generalForm.js" type="text/javascript"></script>
<script src="javascript/postNews.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="./CSS/vacancy.css">
<div id=main>
<br class="bigOnly">
<br class="bigOnly">
<br class="bigOnly">
<br class="bigOnly">

	<div class="w3-container w3-padding-64 w3-center">
	<h1>Upload document to be available for BANES</h1>
	<br> <br> <br>
		<form style="font-size: 25px;" method="post" action="./avonloc.php" enctype="multipart/form-data">
			<input type="hidden" name="command" value="submitBanes"> 
			<input type="hidden" name="action" value="create"> 
			<input type="hidden" name="banesAuthor" value="{$sess_userId}">		
			 Document Title<br> <input class="headline"	type="text" name="banesTitle" value="{$banesTitle}"> <br> <br>
			  <div>
An optional short description<br>
			    <div>
			    <textarea title="A short description of the document"  name="banesDescription" spellcheck="true"  rows ="10" maxlength="500">{$banesDescription}</textarea>
			    </div>
			  </div>
			  			 			<br> <br>
				<span id="uploadMessage" >
{if $fileExists}
				{$errorMessage}
{else}
    			Upload the document in pdf, doc, docx, xls or xlxs format
{/if}	
				</span><br>
			<label id="upload" for="banesDoc"  class="w3-btn w3-theme formSubmit">
    		<input type="file" name="banesDoc" id="fileToUpload" style="display:none;" >
    		Select the pdf, doc, docx, xls or xlxs
    		</label>
    		
    		 <br> <Br> 
			<input  id="postNewsSubmit" type="submit" class="w3-btn w3-theme formSubmit" value="Submit"><br>
			<br>
		</form>
	</div>
	

	<div class="w3-row-padding w3-margin-top w3-center"  style="font-size: 14pt;">
	<h2>Delete redundant BANES documents</h2>
<!-- Basic banes page -->

{foreach from=$banesList key=myId item=i}

		<form style="font-size: 25px;" method="post" action="./avonloc.php" enctype="multipart/form-data">
			<input type="hidden" name="command" value="deleteBanes"> 
			<input type="hidden" name="action" value="delete"> 
			<input type="hidden" name="banesId" value="{$i.banesId}"> 
			<input type="hidden" name="fileName" value="{$i.fileName}"> 
			<a href="./uploads/{$i.fileName}">{$i.banesTitle}</a>
			<input title="delete {$i.banesTitle} => {$i.fileName}"  id="postNewsSubmit"  type="image" src="./images/delete_48.png" class="formSubmit"  width="15"><br>
		</form>
{/foreach}
</div>
	
	
</div>