<script src="javascript/generalForm.js" type="text/javascript"></script>
<script src="javascript/register.js" type="text/javascript"></script>
<script src="javascript/postNews.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="./CSS/register.css">
<div id=main>
<br class="bigOnly">
<br class="bigOnly">

<form action="./avonloc.php" autocomplete="off" method="POST">
<input type="hidden" name="command" value="submitRegistrationRequest">

  <header>
    <h2>Apply for membership of the LOC site</h2>
  </header>
  
  <div>
    <label class="desc" id="titleFirstName" for="firstName">First name</label>
    <div>
      <input title="Your first name" id="firstName" name="firstName" type="text" class="field text fn" value="{$firstName}" size="8"  spellcheck="false" >
     </div>
     </div>
     <div>
      <label class="desc"  id="titleSurName" for="surname">Surname</label>    
      <div> 
      <input title="your surname" id="surname" name="surname" type="text" class="field text fn" value="{$surname}" size="8"  spellcheck="false"  >
    </div>
  </div>
    
  <div>
    <label class="desc" id="titleEmail" for="email" >
      Email
    </label>
    <div>
      <input id="email" name="email" type="email" spellcheck="false" value="{$email}" maxlength="255" spellcheck="false"> 
      <div id="emailMessage" class="warn"><br></div>
   </div>
  </div>
    
  <div>
    <fieldset>
    
      <legend id="title5" class="desc">
        What type of membership are you requesting?
      </legend>
      
      <div>
      	<input id="memberTypeRadio" name="memberType" type="hidden" value="">
      	<div>
      		<input id="memberTypeRadio_0" name="memberType" type="radio" value="Practice_owner"  {if $memberType==Practice_owner} checked="checked"{/if}>
      		<label class="choice" for="memberTypeRadio_0">Practice owner / manager</label>
      	</div>
        <div>
        	<input id="memberTypeRadio_1" name="memberType" type="radio" value="Optometrist"  {if $memberType==Optometrist} checked="checked"{/if}>
        	<label class="choice" for="memberTypeRadio_1">Optometrist</label>
        </div>
        <div>
        	<input id="memberTypeRadio_2" name="memberType" type="radio" value="Dispensing_optician"  {if $memberType==Dispensing_optician} checked="checked"{/if}>
        	<label class="choice" for="memberTypeRadio_2">Dispensing optician</label>
        </div>
        <div>
        	<input id="memberTypeRadio_3" name="memberType" type="radio" value="Contact_lens_optician"  {if $memberType==Contact_lens_optician} checked="checked"{/if}>
        	<label class="choice" for="memberTypeRadio_3">Contact lens optician</label>
        </div>
        <div>
        	<input id="memberTypeRadio_4" name="memberType" type="radio" value="CET_provider" {if $memberType==CET_provider} checked="checked"{/if}>
        	<label class="choice" for="memberTypeRadio_4">CET provider</label>
        </div>
        <div>
        	<input id="memberTypeRadio_5" name="memberType" type="radio" value="admin" {if $memberType==admin} checked="checked"{/if}>
        	<label class="choice" for="memberTypeRadio_5">Site content administrator</label>
        </div>
      </div>
    </fieldset>
  </div>
  
  <div id="GOCNum">
    <label class="desc" id="titleGOC" for="GOCNumber">GOC number</label>
    <div>
      <input title="" id="GOCNumber" name="GOCNumber" type="text" class="field text fn" value="{$GOCNumber}" size="8" >
    </div>
  </div>
  
  <div >
    <label class="desc" for="member">Are you a committee member?</label>
    <div>
      <select id="member" title=""  name="member"  >
      <option value="{$member}">{$member}</option>
      <option value="Yes">Yes</option>
      <option value="No">No</option>
      </select>
    </div>
  </div>
  
  <div id="role">
    <label class="desc" for="role">What is your role on the committee?</label>
    <div>
      <select title=""  name="role"  >
      <option value="{$role}">{$role}</option>
      <option value="Chair">Chair</option>
      <option value="Vice Chair">Vice Chair</option>
      <option value="Treasurer">Treasurer</option>
      <option value="Secretary">Secretary</option>
      <option value="Co-opted">Co-opted</option>
      <option value="Member">Member</option>
      <option value="Observer">Observer</option>
      </select>
    </div>
    
  </div>
  
  <div>
    <label class="desc" id="titleUsername" for="username">User name</label>
    <div>
      <input title="a unique username that to use to login to the site in future" id="username" name="username" type="text" class="field text fn form-control" 
      value="{$username}" 
      spellcheck="false"
      size="8" tabindex="9">
    </div>
  </div>
  
  <div>
    <label class="desc" id="titlePassword" for="password">Password <span id="toggleVis">(show)</span></label>
    <div>
      <input title="Min 8 characters, use length, Uppercase and numbers to strengthen" id="password" name="password" type="password" class="field text fn form-control" value="{$password}" size="8" tabindex="9"> 
      <div id="passMessage" class="warn"><br></div> 
    </div>
  </div>

  <div>
    <label class="desc" id="titlePasswordRpt" for="passwordRpt">Repeat password</label>
    <div>
      <input title="" id="passwordRpt" name="passwordRpt" type="password" class="field text fn" value="{$passwordRpt}" size="8" tabindex="9"> 
      <br><span id="repeatMessage"></span>   
    </div>
  </div>	  
  
  <div>
		<div id="registerButtonDiv">
  		<input id="saveForm" name="saveForm" type="submit" class="w3-btn w3-theme" value="register">
  		<br><span id = "allFieldsWarning">All fields must contain valid entries before you can register</span>
  		{if $usernameTaken eq "true"}
      <span id= "userNameTaken" ><br>Sorry the username {$userName} is taken please choose again</span>
      {/if}
  		
    </div>
	</div>
  
</form>
</div>


