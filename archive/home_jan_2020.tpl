
<link rel="stylesheet" type="text/css" href="./CSS/home.css">
<link rel="stylesheet" type="text/css" href="./CSS/vacancy.css">
<div class="w3-image w3-animate-opacity">
<!--   <img class="" src="images/avonloceye.jpeg" alt="glasses" style="width:100%;min-height:550px;max-height:1300px;"> -->
  <!--  img class="" src="images/glassesandpen.jpg" alt="glasses" style="width:100%;min-height:550px;max-height:1300px;"-->
  <img class="" src="images/cbridge.jpg" alt="glasses" style="width:100%;min-height:550px;max-height:1300px;">
  <div id="avonLocBanner" class="w3-title w3-margin-32" >  
    <div  class="w3-animate-opacity w3-btn w3-xlarge w3-theme" style="cursor:default;position:relative;top:20px;" title="">Avon LOC{if $sess_approval=="pending"}<br><I>(approval pending)</I> {/if}</div>
  </div>
</div>

<br>
<div class="w3-container w3-padding-16 w3-center">
<h2><a href="./uploads/Avon LOC Data Protection and Privacy Policy (May 2018).do
cx" target="_blank">Download our data protection policy</a></h2>
<!-- <h2>Meet our officers</h2>	 -->
				
<h1><a href="./avonloc.php?command=about">About us</a></h1>
{if $sess_approval eq "approved"}
<h1><a href="./avonloc.php?command=contact">Contact</a></h1>
{/if}

<h2> The latest news and CET courses</h2>
<!-- The doReadNews(2) is called after this which insertst the contents of readNews.tpl  -->

{foreach from=$articleList key=myId item=i}
<div class="w3-half w3-row-padding w3-margin-top">
<div class="w3-card-2 _vacancyCard  w3-white " >
<div >
        <div style="height:300px;padding:0 8px;overflow: hidden;">
        <h2>{$i.headline}</h2>
        <h4>Published on {$i.published}</h4>
        <span style="display:inline;font-size:20px;" id="newsAuthor">by {$i.fullName}. </span>		
        {$i.newsDescription}<BR>
        </div>
        <footer class="w3-light-blue w3-container">
        <p>For more information, login and visit the news page
        </footer>

</div>
</div>
</div>
{/foreach}

{foreach from=$courseList key=myId item=i}
<div class="w3-half w3-row-padding w3-margin-top">
<div class="w3-card-2  w3-white ">
<div >
        <div style="height:300px;padding:0 8px;overflow: hidden;">
        <h2>{$i.courseName}</h2>
      <div class="w3-container">
      {$i.courseShortDesc}<br>
      <br>
      CLO:{$i.clo}
      &nbspDO:{$i.do}
      &nbspOO:Yes<br>
      points:{$i.coursePoints}<br><br>
Date(s) available {$i.courseDate}<br><br>

{if ($i.url)}
Website:<a href={$i.url}>{$i.url}</a>
{/if}
</div>
</div>
      <footer class="w3-light-blue w3-container">
<!--         <p>To apply contact {$i.fullName} at <a href="mailto:{$i.courseEmail}">{$i.courseEmail}</a></p> -->
        <p>For more information, login and visit the education pages</p>
      </footer>

</div>
</div>
</div>
{/foreach}

