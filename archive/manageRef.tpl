<script src="javascript/generalForm.js" type="text/javascript"></script>
<script src="javascript/postNews.js" type="text/javascript"></script>
<!-- <link rel="stylesheet" type="text/css" href="./CSS/vacancy.css"> -->
<!-- <link rel="stylesheet" type="text/css" href="./CSS/register.css"> -->
<div id=main>
<br class="bigOnly">
<br class="bigOnly">
<br class="bigOnly">
<br class="bigOnly">

	<div class="w3-container w3-padding-64 w3-center">
	<h1>Upload referral guidelines</h1>
	<br> <br> <br>
		<form style="font-size: 25px;" method="post" action="./avonloc.php" enctype="multipart/form-data">
			<input type="hidden" name="command" value="submitGuideline"> 
			<input type="hidden" name="action" value="create"> 
			<input type="hidden" name="guidelineAuthor" value="{$sess_userId}">		
			 Document Title<br> 
			<input class="headline"	type="text" name="guidelineTitle" value="{$guidelineTitle}"> 
			<br> <br>
			  <div>
Short Description<br>
			    <div>
			    <textarea title="A short description of the document"  name="guidelineDescription" spellcheck="true"  rows ="10" maxlength="500">{$guidelineDescription}</textarea>
			    </div>
			  </div>			
			<br><br>
				<span id="uploadMessage" >
{if $fileExists}
				{$errorMessage}
{else}
    			Upload the document in pdf, doc or docx format
{/if}				
				</span><br>

			<label id="upload" for="guidelineDoc"  class="w3-btn w3-theme formSubmit">
    		<input type="file" name="guidelineDoc" id="fileToUpload" style="display:none;" >
    		Select the pdf, doc or docx
    		</label>
   		
    		 <br> <Br> 
			<input  id="postNewsSubmit" type="submit" class="w3-btn w3-theme formSubmit" value="Submit"><br>
			<br>
		</form>
	</div>	
	

	<div class="w3-row-padding w3-margin-top w3-center"  style="font-size: 14pt;">
	<h2>Delete redundant guidelines</h2>
<!-- Basic download page -->

{foreach from=$guidelineList key=myId item=i}

		<form style="font-size: 25px;" method="post" action="./avonloc.php" enctype="multipart/form-data">
			<input type="hidden" name="command" value="deleteGuideline"> 
			<input type="hidden" name="action" value="delete"> 
			<input type="hidden" name="guidelineId" value="{$i.guidelineId}"> 
			<input type="hidden" name="fileName" value="{$i.fileName}"> 
			<a href="./uploads/{$i.fileName}">{$i.guidelineTitle}</a>
			<input title="delete {$i.guidelineTitle} => {$i.fileName}"  id="postNewsSubmit"  type="image" src="./images/delete_48.png" class="formSubmit"  width="15"><br>
		</form>
{/foreach}
</div>	
	
</div>