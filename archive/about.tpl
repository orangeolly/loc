<link rel="stylesheet" type="text/css" href="./CSS/about.css">
<div class="w3-container w3-padding-64 w3-center" style="margin: 0px auto;font-size:25px;max-width:1000px;">
<br class="bigOnly">
<br class="bigOnly">
<br class="bigOnly">

<h1>ABOUT THE LOC</h1>

<p>
Avon LOC consists of up to 12 members. Elections for vacancies on the LOC take place at the Annual General Meeting, and a term of office is 3 years.</p>
<p>
In addition, the LOC has 2 observers, being a hospital representative and an optometric advisor.  While these two observers take part in discussions, they do not have any vote within the LOC.</p>
<p>
<a href="./uploads/theLOCConstitution.pdf">The LOC Constitution</a>
</p>

<p>
Members are listed below, along with their LOC e-mail addresses:-</p>


<table >
{foreach from=$members key=myId item=i}
<tr><th  style="text-align:Center;">{$i.role}</th></tr>
{if $i.picName}<tr><td><img src="{if $i.picName}./uploads/{$i.picName}{else}./images/cartoon.gif{/if}" alt="{$role}" style="width:150px;height:150px;" ></td></tr>{/if}
<tr><td style="text-align:Center;">{$i.fullName}</td></tr>
<tr><td style="display:block;overflow;hidden;"> <a href="mailto:{$i.email}">{$i.email}</a></td></tr>
<tr><td>&nbsp</td></tr>
{/foreach}
</table>
</div>
