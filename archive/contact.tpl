<link rel="stylesheet" type="text/css" href="./CSS/about.css">
<div class="w3-container w3-padding-64 w3-center" style="margin: 0px auto;font-size:25px;max-width:1000px;">
<br class="bigOnly">
<br class="bigOnly">
<br class="bigOnly">

<h1>Contact Avon LOC</h1>

<p>
If you would like to receive information and updates from Avon LOC by email, please email us with your name, GOC number and practice details to: 
<a href="mailto:secretary@avonloc.co.uk">secretary@avonloc.co.uk</a>

<br>
This will subscribe you to our external mailing list informing of any key news of information. This will also be available on the website.
<p>
In addition you can subscribe to be informed about new news articles and courses.


<div class="w3-container w3-padding-16">
You are {if $reader.subscribedToNews == 0}not{/if} currently registered to receive emails when new articles are posted.
 {if $reader.subscribedToNews == 0}<a href="./avonloc.php?command=subscribeNews">Subscribe?</a>{else}
 <a  href="./avonloc.php?command=unsubscribeNews">Unsubscribe</a>
 {/if}
 <br>
 You are {if $reader.subscribedToCourses == 0}not{/if} currently registered to receive emails when new courses are posted.
 {if $reader.subscribedToCourses == 0}<a href="./avonloc.php?command=subscribeCourses">Subscribe?</a>{else}
 <a  href="./avonloc.php?command=unsubscribeCourses">Unsubscribe</a>
 {/if}
 </div>
 
<P>
If you have any queries you can contact us through our chair: 
<a href="mailto:chair@avonloc.co.uk">chair@avonloc.co.uk</a>.
</P>


</div>
