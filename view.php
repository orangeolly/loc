<?php
require_once('avonLocDataLayer.php');
require_once('dd.php');

class view
{
	var $log;

	var $HEADER = "header2.tpl";
	var $MENU =  "navigation.tpl";
	var $FOOTER =  "footer2.tpl";
	var $HOME = "home.tpl";
	var $PRACTICEINFO = "avonLocPracticeInfo.tpl";
	var $ABOUT = "about.tpl";
	var $NEWS = "avonLocNews.tpl";
	var $DOWNLOADS = "avonLocDownloads.tpl";
	var $LOGIN = "login.tpl";
	var $REGISTER = "register.tpl";
	var $VACANCIES = "vacancies.tpl";
	var $EMPLOYEES = "employees.tpl";
	var $POSTVACANCIES = "postVacancies.tpl";
	var $POSTSERVICES = "postServices.tpl";
	var $POSTPRACTICE = "postPractice.tpl";
	var $VIEWPRACTICES = "practiceInfo.tpl";
	var $READNEWS = "readNews.tpl";
	var $POSTNEWS = "postNews.tpl";
	var $ADDCOURSE = "addCourse.tpl";
	var $VIEWCOURSES = "viewCourses.tpl";
	var $MANAGEGUIDELINES = "manageRef.tpl";
	var $VIEWGUIDELINES = "viewGuidelines.tpl";
	var $MANAGEDOWNLOADS = "manageDownloads.tpl";
	var $VIEWDOWNLOADS = "viewDownloads.tpl";
	var $MANAGEBANES = "manageBanes.tpl";
	var $VIEWBANES = "viewBanes.tpl";
	var $MANAGELINKS = "manageLinks.tpl";
	var $VIEWLINKS = "viewLinks.tpl";
	var $MAINTAINAPPLICANTS = "manageApplicants.tpl";
	var $MAILINGLIST = "mailingList.tpl";
	VAR $UPLOADCOMMITTEEPICS = "uploadCommitteePics.tpl";
	var $REGSUCCESS = "regSuccess.tpl";
	var $FORGOTPASSWORD = "forgotPassword.tpl";
	var $CONTACT = "contact.tpl";
	var $SUBSCRIBE = "subscribe.tpl";
	
	function __construct()
	{	
		if(true)
		{
			error_reporting(E_ALL-E_NOTICE);
			ini_set('display_errors',1);
		}
		$this->dataLayer = new dataLayer();		
		$this->log = Logger::getLogger('avonLocView');
		if (DEBUG) echo(" view logger should be running now");
		$this->log->debug(" avonLocView debugger running ");
		$this->smarty = new Smarty;
		$this->smarty->compile_dir = SMARTY_COMPILE_DIR;
		$this->smarty->template_dir = "./templates";		
	}
	
	function Header($allow)
	{
		
		$this->log->trace(" in header with allow = $allow");
		//$this->smarty->display ($this->MENU);
		$agent = $_SERVER['HTTP_USER_AGENT'];
		
		if (preg_match("(iPhone|BlackBerry|PalmSource)", $agent) != false) {
			$this->smarty->assign("device","handheld");
			$this->log->trace(" handheld device ");
		}
		else {
// 			echo "<!-- not mobile -->";
			$this->smarty->assign("device","normal");
			$this->log->trace(" normal  device ");
		}
		//$this->showVariables(false);
		//$this->smarty->assign("device","handheld");
		//$this->log->trace(" overwritten device to be handheld ");
		$this->smartySession();
//  		$this->smarty->assign("authType",$_SESSION['authType']);
  		//$this->smarty->assign("sess_authLevel",$_SESSION['authLevel']);
		$this->smarty->display($this->HEADER);
	}
	
	function Menu()
	{
		$this->smarty->display ($this->MENU);
	}
	
	function Footer()
	{
		$this->smarty->display ($this->FOOTER);
	}


	function showVariables($screen)
	{
		$this->log->info('*********** Request data**************');
		foreach($_REQUEST as $key=>$val)
		{
			$this->log->info('Key:'.$key.' value:'.$val);
		}
		$this->log->info('**********Cookie data*************');
		foreach($_COOKIE as $key=>$val)
		{
			$this->log->info('Key:'.$key.' value:'.$val);
		}		
		$this->log->info('***********Session data**************');
		foreach($_SESSION as $key=>$val)
		{
			$this->log->info('Key:'.$key.' value:'.$val);
		}
		if ($screen)
		{
		echo('<br><b>Request data</b>');
		foreach($_REQUEST as $key=>$val)
			{
				echo('<br>Key:'.$key.' value:'.$val);
			}
		echo('<br><br><b>Cookie data</b>');
		foreach($_COOKIE as $key=>$val)
			{
				echo('<br>cookie Key:'.$key.' value:'.$val);
			}
		echo('<br><br><b>Session data</b>');
			foreach($_SESSION as $key=>$val)
			{
				echo('<br>Session Key:'.$key.' value:'.$val);
			}
		echo("<br>");
		}
		$this->log->info('***********end data**************');		
	}
	
	function smartySession()
	{
		foreach($_SESSION as $key=>$val)
		{
			$this->smarty->assign("sess_".$key,$val);
		}
	}
	
	function getRequestKeyNo($no)
	{
		$count =0;
		foreach($_REQUEST as $key=>$val)
		{
			$count = $count+1;
			if ($count==$no)
			{
				return $no;
			}
		}
	}
	
	function showItemContents($item)
	{
		$this->log->trace('************in showItemContents() Object from table :'.$item['tableName'].'************');
		if($item['tableName'])
		{
			$columns = $this->dataLayer->findSchemaColumns($item['tableName']);
			$i=0;
	
			while($columns[$i]<>null)
			{
				$this->log->trace($columns[$i]['COLUMN_NAME']."=".$item[$columns[$i]['COLUMN_NAME']]."<br>");
				$i++;
			}
		}
		else
		{
			$this->log->trace("<br> This object cannot be auto echoed as the table name is not defined. It should have been created using dao findItem()<br>");
		}
		$this->log->trace('*************done showItemContents() Object from table :'.$item['tableName'].'*********');
	}
	
	/// Purely for debugging purposes
	
	function show($item)
	{
		echo '<pre><br><br><br>';
		var_dump($item);
		echo '</pre>';		
	}
	
	function fillRequestItemThenSession($item,$useNulls)
	{
		// this function takes an array that already know's its table name
		// e.g declare the array  as $impact['tableName']='InitiativeImpact';
		// or get using findItem()
		$this->log->trace("in fillRequestItemThenSession() :".$item['tableName'].'');
		$columns = $this->dataLayer->findSchemaColumns($item['tableName']);
		$i=0;
	
		while($columns[$i]<>null)
		{
			// if there is a value in the request object, or we allow the request to speficy nulls
			// otherwise we don't want to overwite data if the request does not have
			// a new value to update.
			if (isset($_REQUEST[$columns[$i]['COLUMN_NAME']]) || $useNulls )
			{
				$columnNameString = $columns[$i]['COLUMN_NAME'];
				$escColValue = $this->dataLayer->mysqli->real_escape_string($_REQUEST[$columnNameString]);
				$item[$columnNameString] =  $escColValue;
			}
			else 
			{
				if (isset($_SESSION[$columns[$i]['COLUMN_NAME']]) || $useNulls )
				{
					$columnNameString = $columns[$i]['COLUMN_NAME'];
					$escColValue = $_SESSION[$columnNameString];
					$item[$columnNameString] =  $escColValue;
				}				
			}
			$i++;
		}
	
		return $item;
	}
	

	function refreshDataLayer($tableName,$keyName)
	{

		$this->log->debug("Entered refreshDataLayer for table ".$tableName." with key  ".$keyName);
		$this->log->info("Entered refreshDataLayer for table ".$tableName." with key  ".$keyName);
		$action = $_REQUEST['action'];
		$this->log->trace("action is ".$action);
	
		if (null==$keyName)
		{
			$this->log->debug("<br>getting key for ".$tableName." in the ".DB_NAME." database. <br>");
			$keyName = $this->dataLayer->findSchemaKeyName($tableName,null);
			$this->log->debug(" key was ".$keyName." <br>");
		}
		$item['tableName']=$tableName;
		$item = $this->fillRequestItemThenSession($item, false);
		// exit if no action needed.
		if (! (($action=='create')|| ($action=='update') || ($action=="createOrUpdate")|| ($action=="delete")) )
		{
			$this->log->debug("<br> no update or creation requested just returning key field value for requested object:".$item[$keyName]." end");
			return $item[$keyName];
		}
		
		if ($action=="delete")
		{
			$this->log->debug("deleting a record from table $tableName with key=".$item[$keyName]);
			$this->dataLayer->removeItemDBEntry($item);
			if (isset($_SESSION[$keyName])){
				unset($_SESSION[$keyName]);
				$this->log->debug("******WARNING, unsetting the session variable for $keyName as the entry in $tableName has been deleted.  *************");
			}
			return;
		}

		if ($action=="createOrUpdate")
		{
			if ( isset($item[$keyName]))
			{
				$this->log->trace(" Create or update chosen Update with key $keyName and key value=".$item->keyName);
				$action="update";
			}
			else
			{
				$this->log->trace(" Create or update chosen Create as key $keyName is not set in sesssion or request");
				$action="create";
			}
		}		
			

		if ($action=='create')
		{
			$item['Id'] = $this->dataLayer->makeAndRefreshItem($item, false, true);
			$this->log->debug("key is ".$item['Id']." end of key");
			return $item['Id'];
		}
		if ($action=='update')
		{
			$this->log->debug("about to update ".$item['tableName']);
			$this->log->trace("  update chosen Update with key $keyName and key value=".$item->keyName);
			$this->log->debug("about to update");
			$this->showItemContents($item);
			$this->log->trace("got 0");
			$item = $this->dataLayer->findItem($item['tableName'],$item[$keyName]);
// 			echo("<br><br><br>before<br>");
// 			var_dump($item);
// 			echo("<br>");
			/// just for the item's reloaded from the database that may contain 
			/// characters that need escaping
			foreach($item as $key => $value)
			{
				$value = $this->dataLayer->mysqli->real_escape_string($value);
				$item[$key] = $value;
			}
// 			echo("<br><br><br>fixed<br>");
// 			var_dump($item);
// 			echo("<br>");
			/// if these items are going back into the database then if they have quotes in strings they need to be escaped.
			$this->log->trace("got 1");
			$item = $this->fillRequestItemThenSession($item, false);
// 			echo("after<br>");
// 			var_dump($item);
// 			echo("<br>");
			$this->log->trace("got 2");
			$this->showItemContents($item);
			$this->log->trace("got 3");
			$this->dataLayer->makeAndRefreshItem($item, true, false);
			$this->log->trace("got 4");
				
			$this->log->debug("key is ".$item['Id']."=".$item[$keyName]." end of key");
			return $item[$keyName];
		}
		return $item[$keyName];
	}	
	

	/// This function will return false if no file was uploaded
	/// "success" if a file matching the criteria was loaded
	/// An error message otherwise.
	function upload($types,$isImage,$maxSizeBytes,$inputName,$maxSizeMessage)
	{
		if (count($_FILES)==0)
		{
			return false;
		}
		if ($_FILES[$inputName]['error']==1)
		{
			$returnMessage= "Sorry, there was a problem with that file. Possibly that the file is too big and would make the site too slow for other users. The limit is $maxSizeBytes bytes";
			if (isset($maxSizeMessage))
			{
				$returnMessage = $maxSizeMessage;
			}
			$this->log->debug($returnMessage);
			return $returnMessage;			
		}
		if ( !isset($_FILES[$inputName]["tmp_name"]) || $_FILES[$inputName]["tmp_name"] == "" )
		{
			$returnMessage= "Sorry but no file was uploaded";
			$this->log->debug($returnMessage);
			return $returnMessage;			
		}
		//$this->show($_FILES);
	
		$this->log->trace("in upload($types,$isImage,$maxSizeBytes,$inputName) trying to load file ".$_FILES[$inputName]["name"].$_FILES[$inputName]["tmp_name"]);
		$target_dir = "uploads/";
		$target_file = $target_dir . basename($_FILES[$inputName]["name"]);
		$uploadOk = 1;
		$returnMessage = "";
		if ($isImage)
		{
			$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
			// Check if image file is a actual image or fake image
			if(true) {
				$check = getimagesize($_FILES[$inputName]["tmp_name"]);
				if($check !== false) {
					//echo "File is an image - " . $check["mime"] . ".";
					$uploadOk = 1;
				} else {
					$returnMessage= "Sorry, that file was not an image.";
					$this->log->debug($returnMessage);
					return $returnMessage;
				}
			}
		}
		// Check if file already exists
		if (file_exists($target_file)) {
			$returnMessage= "That file '$target_file' already exists, try renaming it.";
			$this->log->debug($returnMessage.$_FILES[$inputName]["name"]);
			return $returnMessage;
		}
		// Check file size
		if ($_FILES[$inputName]["size"] > $maxSizeBytes) {
			$returnMessage= "Sorry, that image is too big and would make the site too slow for other users. The limit is $maxSizeBytes bytes";
			if (isset($maxSizeMessage))
			{
				$returnMessage = $maxSizeMessage;
			}
			$this->log->debug($returnMessage);
			return $returnMessage;
		}
	
		// check the extension
		$fileName = $_FILES[$inputName]["name"];
		$dotPos = strpos($fileName,'.');
		$extension = substr($fileName,$dotPos+1);
		if (!in_array($extension,$types) && !$isImage)
		{
			$returnMessage= "File is of wrong type, $extension was supplied but that is not supported";
			$this->log->debug($returnMessage);
			return $returnMessage;
		}
	
	
		if (move_uploaded_file($_FILES[$inputName]["tmp_name"], $target_file)) {
				
			$this->log->trace("The file ". basename( $_FILES[$inputName]["name"]). " has been uploaded.");
			return "success";
		} else {
			$returnMessage= "Sorry, there was an error uploading your file.";
			$this->log->debug($returnMessage);
			return $returnMessage;
		}
	}
	
	function deleteUploadDoc($docName)
	{
		$target_dir = "uploads/";
		$target_file = $target_dir . $docName;	
		return unlink($target_file);
	}
	
	function generateHash($password)
	{
		// A higher "cost" is more secure but consumes more processing power
		$cost = 10;
		
		// Create a random salt
		//$salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');
		$salt = strtr(base64_encode(openssl_random_pseudo_bytes(16, $wasItStrong)), '+', '.');
		if (DEBUG) $this->log->trace("salt strength was $wasItStrong");
		// Prefix information about the hash so PHP knows how to verify it later.
		// "$2a$" Means we're using the Blowfish algorithm. The following two digits are the cost parameter.
		$salt = sprintf("$2a$%02d$", $cost) . $salt;
		
		// Hash the password with the salt
		$hash = crypt($password, $salt);
		if (DEBUG) $this->log->debug($password." salt:".$salt." hash:".$hash);
		$this->log->trace("new hash is $hash");
		return $hash;
	}
	/**************************************************************************************************
	 * 						Registration, login and logout
	 * ***********************************************************************************************
	 */
	
	function Register($firstTime,$status="")
	{
        $this->smarty->assign("status",$status);
        $this->smarty->assign("RECAPTCHA_PUBLIC_KEY",RECAPTCHA_PUBLIC_KEY);
		$this->log->debug("in the registration page first time val is $firstTime");
		if ($firstTime)
		{ // a default value.
			$this->log->trace(" its the first visit to the registration page ");
			$this->smarty->assign("memberType","Practice_owner");
			$this->smarty->assign("usernameTaken","false");
			$this->smarty->assign("username","");
			$this->smarty->assign("email","");
			$this->smarty->assign("GOCNumber","");
			$this->smarty->assign("fullName","");
			$this->smarty->assign("firstName","");
			$this->smarty->assign("surname","");
			$this->smarty->assign("password","");
			$this->smarty->assign("passwordRpt","");
		}
		$this->smarty->display ($this->REGISTER);
	}
	
	function SubmitRegistrationRequest()
	{
        $captureStatus = $this->verifyCaptcha();
        $this->log->debug($captureStatus);
        if ($captureStatus != "success")
        {
            return false;
        }
		$email = $_REQUEST["email"];
		//$name = $_REQUEST["fullName"];
		$GOCNumber = $_REQUEST["GOCNumber"];
		if (
// 				!strpos($name,' ')
// 				||
				strpos($email,'.ru') == strlen($email)-3
				)
		{
			/// should elimiate many hack attempts without feedback to the attacker.
		    $this->log->trace("Registration simply terminated due to possible hacking attempt as email looked Russian");
			return true;
		}
		$this->log->trace("In registrationRequest()");
		$username= $_REQUEST["username"];
		$escUsername = $this->dataLayer->mysqli->real_escape_string($username);
		$password = $_REQUEST["password"];
		$results = $this->dataLayer->findItemsWithFilter("registereduser", "username='$escUsername'", "username", null);
		if (isset($results))
		{
    		$this->log->trace("User rows found = ".count($results));
    		if (count($results)>0)
    		{
    			$this->smarty->assign("usernameTaken","true");
    			$this->smarty->assign("username",$username);
    			$this->smarty->assign("email",$_REQUEST["email"]);
    			$this->smarty->assign("GOCNumber",$_REQUEST["GOCNumber"]);
    			$this->smarty->assign("fullName",$_REQUEST["fullName"]);
    			$this->smarty->assign("firstName",$_REQUEST["firstName"]);
    			$this->smarty->assign("surname",$_REQUEST["surname"]);
    			$this->smarty->assign("memberType",$_REQUEST["memberType"]);
    			return false;
    		}
		}
		else{
		    $this->log->trace("No user currently exists with username $escUsername");
		}
// 		// A higher "cost" is more secure but consumes more processing power
// 		$cost = 10;
	
// 		// Create a random salt
// 		$salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');
	
// 		// Prefix information about the hash so PHP knows how to verify it later.
// 		// "$2a$" Means we're using the Blowfish algorithm. The following two digits are the cost parameter.
// 		$salt = sprintf("$2a$%02d$", $cost) . $salt;
	
// 		// Hash the password with the salt
// 		$hash = crypt($password, $salt);
// 		$this->log->debug($password." salt:".$salt." hash:".$hash);
// 		$this->log->trace("new has is $hash");
		
		$hash = $this->generateHash($password);
		
		/// Ranks roles in order for later retrieval in order
		switch($_REQUEST['role'])
		{
		case "Chair":
			$_REQUEST['rank']=1;
			break;
			
		case "Vice Chair":
			$_REQUEST['rank']=2;
			break;
			
		case "Treasurer":
			$_REQUEST['rank']=3;
			break;
			
		case "Secretary":
			$_REQUEST['rank']=4;
			break;
			
		case "Co-opted":
			$_REQUEST['rank']=5;
			break;
			
		case "Member":
			$_REQUEST['rank']=6;
			break;
			
		case "Observer":
			$_REQUEST['rank']=7;
			break;
		}
	
		$_REQUEST["hash"]=$hash;
		$_REQUEST["action"]="create";
		$_REQUEST["approval"]="pending";
		$_REQUEST["delay"]=0.25;
		$_REQUEST['fullName']=$_REQUEST['firstName']." ".$_REQUEST['surname'];
		$this->refreshDataLayer("registereduser", "userId");
		
		
		$subscriberList = $this->dataLayer->findItemsWithFilter("registereduser", "memberType='admin' and approval='approved'", null, null);
		//dd($subscriberList);
		$headers = 'From: newUserRequests@avonloc.co.uk';
		$subject = "New user applied: ".$_REQUEST['fullName'];
		//$user=$this->dataLayer->findItem('registereduser', $_SESSION['userId']);
		$message = $_REQUEST['fullName']." has just applied for membership of the avonloc web site. Visit https://www.avonloc.co.uk, login and choose 'Reference info' then 'Maintain applications' to approve or reject this appliation";
		if (is_array($subscriberList) || is_object($subscriberList))
		{
			foreach ($subscriberList as $subscriber)
			{
				mail($subscriber['email'],$subject,$message,$headers);
			}
		}
		//$this->TryLogin();
		return true;
	}
	
	function ShowRegSuccess()
	{
		$this->smarty->display($this->REGSUCCESS);
	}
	
	function TryLogin()
	{
		/// Need to protect against intrusion attempts and DNS attempts.
		/// Intrusion may be from the same IP, which is easier to detect and can be managed at the session level.
		/// In this case, checking does not need to impact the database and therefore should be eliminated first.
		/// If the attack is not coming from the same IP address, then I need to check repeated attempts at the same user at the database level.
		/// If the user does not exist then this is not a worry? Although the timing will not increase.
		/// The final type of attack to prevent is from different IP addresses to all users.
		/// This last one is particularly nasty as if you try all users with the top 100 passwords, then you are going to get some hits.
		/// This last one has not been implemented yet. It would require the bots to know the user names of all users.
	
		/// Initially set the login delay to a value and double it on each failed attempt in a session.
		

		$roundingFactor = 4;
		if(!isset($_SESSION["logindelay"])){
			$this->log->trace(" current session appears to not be set. Delay is: ".$_SESSION["logindelay"]);
			$_SESSION["logindelay"]=0.25;
			$_SESSION['lastLoginAttempt']=0;
		}
		$sessionStillToWait =($_SESSION['lastLoginAttempt'] + $_SESSION['logindelay'])-time();
		$this->log->debug("in doTryLogin() session last attempt =".$_SESSION['lastLoginAttempt']." delay =".$_SESSION['logindelay']." to wait =".$sessionStillToWait);
		$username = $_REQUEST["username"];
		$escUsername = $this->dataLayer->mysqli->real_escape_string($username);
		$password = $_REQUEST["password"];
	
		if ($sessionStillToWait >0)
		{
			// fail because the session is trying repeated logins.
			// this is a first defense against DNS that does not touch the database.
			$this->log->debug(" session login attempt blocked at time:".strval(time())." Session delay is now: ".$_SESSION["logindelay"]." user was never checked");
			return false;
		}
		$this->log->trace(" got past session timeout block ");
		$this->log->trace(" username from request is :$username password is :hidden escaped username is:$escUsername");
	
		/// This username may never have been registered. In which case nothing will come back.
		$user = $this->dataLayer->getUserData($escUsername);
		
		if ($_REQUEST['login'] == "Forgotten Password?")
		{
			$this->LostPassword($user,$escUsername,$user["email"]);
			return;
		}
		
		$userExists = false;
		if (isset($user['userId']))
		{
			$userExists = true;
			$this->log->trace(" The user $username exist in the database with a delay of:".$user['delay']);
			if ($userExists && !isset($user['lastLoginAttempt'])){
				$user['lastLoginAttempt'] = 0;
			}
			$dbBasedWait =  $user['lastLoginAttempt']+$user['delay']-time();
			$this->log->trace(" time is :".time()." last attempt from db was :".$user['lastLoginAttempt']." last delay from db was: ".$user['delay']." Still to wait from db is ".$dbBasedWait);
		
			if ($dbBasedWait >0)
			{
				$this->log->debug(" session login attempt blocked at time:".strval(time())." Session delay is now: ".$_SESSION["logindelay"]." user delay is now "+ $user['delay']);
				//return $user['delay'];
				return false;
				// fail because the database says someone is avoiding sessions but still trying.
			}
		
		}
		else 
		{
			$this->log->trace(" The user $username does not exist in the database");				
		}
	
		$userhash= $user['hash'];
		$guesshash = crypt($password, $user['hash']);
		$this->log->trace(" hash from db is :$userhash, guess has is:$guesshash");
	
		//echo("user hash is :".$userhash);
		//echo("<br>guess hash is :".$userhash.'<br>');
		//var_dump($userhash == $guesshash );
		/// successfully logged in
		/// this can only happen if the user exists.
		if ( $userhash == $guesshash ) {
			$this->log->trace(" password did  match ");
			$_SESSION["username"]=$username;
			$_SESSION['login_time'] = time();
			$_SESSION['logindelay'] = 0.25; // need to set this in the database as well.
			$_SESSION['authLevel'] = 2;
			//$_SESSION['authType'] = "admin";
			$_SESSION['userId'] = $user['userId'];
			$_SESSION['memberType'] = $user['memberType'];
			$_SESSION['email'] = $user['email'];
			$_SESSION['approval'] = $user['approval'];
			$_SESSION['fullName'] = $user['fullName'];
			$_SESSION['rank'] = $user['rank'];
			$this->dataLayer->updateLoginAttemptTime($escUsername,time(),0.25);
			$this->log->trace(" session allowed in at time:".strval(time()));
			//return 0;
			return true;
		}
		else { /// failed to log in
			/// could be here even if the username did not exist.
			$this->log->trace(" password did not match");
			if ($userExists) 
			{
				$this->log->trace(" user delay is currently:".$user['delay']);
				$this->dataLayer->updateLoginAttemptTime($escUsername,time(),$user['delay']*2);
			}
			
			// just in case it somehow gets stuck at 0.
			if ($_SESSION["logindelay"]<0.25)
			{
				$this->log->trace(" Session delay:".$_SESSION["logindelay"]." was less than the initial delay:0.25 so setting it to the intial delay");
				$_SESSION["logindelay"]=0.25;
			}
			else 
			{
				$this->log->trace(" Session delay:".$_SESSION["logindelay"]." was greater or equal to initial delay:0.25");				
			}
			
			$this->log->trace(" current session delay is now: ".$_SESSION["logindelay"]);
				
			$_SESSION['lastLoginAttempt']= time();
			if(!isset($_SESSION["logindelay"])){
				$_SESSION["logindelay"]=0.25;
				$this->log->trace(" Set session delay to : ".$_SESSION["logindelay"]);
				
			}
			else {
				$_SESSION["logindelay"]=$_SESSION["logindelay"]*2;
				$this->log->trace(" Doubled session delay to: ".$_SESSION["logindelay"]);
				
			}
			$maxOfBothDelay = $_SESSION["logindelay"];
			if ($userExists && $user['delay']> $_SESSION["logindelay"])
			{
				$maxOfBothDelay = $user['delay'];
				$this->log->trace(" Database delay was greater than session DB= ".$user['delay']." Session was:".$_SESSION["logindelay"]);
				
			}
			$_SESSION["logindelay"]=$maxOfBothDelay;
			$this->log->debug(" session login attempt blocked at time:".strval(time())." Session delay is now: ".$_SESSION["logindelay"]." user delay is now ".$user['delay']);		
			return false;
		}
	}
	
	function Login($justFailed)
	{
		$this->log->trace("last:".$_SESSION['last']." test:".$_SESSION['test']);
		$sessionStillToWait =($_SESSION['lastLoginAttempt'] + $_SESSION['logindelay'])-time();
		$this->log->trace("in doLogin() delay =".$_SESSION['logindelay']." session to wait =".$sessionStillToWait);
		if ($_SESSION["logindelay"]>2)
		{
			$stillToGo = round ($_SESSION["lastLoginAttempt"]+$_SESSION["logindelay"]-time(),1);
			
			$this->log->trace("Still to go = ".$stillToGo);
			$this->smarty->assign("timeToGo",$stillToGo);
			$this->smarty->assign("showLoginDelay","true");
		}
		$this->smarty->assign("username",$_REQUEST['username']);
		if ($justFailed)
		{
			$this->smarty->assign("showTryAgain","true");
		}
		$this->smarty->display($this->LOGIN);		
	}
	
	
	function Logout()
	{
		$this->log->trace(" Logging out ");
		unset($_SESSION["username"]);
		unset($_SESSION['login_time']);
		unset($_SESSION['authLevel']);
		//unset($_SESSION['authType']);
		unset($_SESSION['userId']);
		unset($_SESSION['memberType']);
		unset($_SESSION['email']);
		unset($_SESSION['approval']);
		unset($_SESSION['fullName']);
		unset($_SESSION['email']);
		unset($_SESSION['employeeId']);
		unset($_SESSION['practiceId']);
		unset($_SESSION['rank']);
		
		$this->log->trace(" session logged out at time:".strval(time()));
	}

	function verifyCaptcha()
    {
        // Validate reCAPTCHA box
        $statusMsg = "unknown";
        if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])){
            // Google reCAPTCHA API secret key
            $secretKey = RECAPTCHA_PRIVATE_KEY;

            // Verify the reCAPTCHA response
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secretKey.'&response='.$_POST['g-recaptcha-response']);

            // Decode json data
            $responseData = json_decode($verifyResponse);

            // If reCAPTCHA response is valid
            if($responseData->success){
                $statusMsg = "success";
            }else{
                $statusMsg = 'Robot verification failed, please try again.';
            }
        }else{
            $statusMsg = 'Please check on the reCAPTCHA box.';
        }
        $this->log->trace("capture status is:".$statusMsg);
        return $statusMsg;
    }
	
	function LostPassword($user,$escusername,$email)
	{
		/// generate a random password
		$password = "";
		$chars = "abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ_!-123456789";
		$charArray = str_split($chars);
		for($i = 0; $i < 8; $i++){
			$randItem = array_rand($charArray);
			$password .= "".$charArray[$randItem];
		}
		
		$hash = $this->generateHash($password);
		$this->dataLayer->updatePasswordForUsername($escusername,$hash);
		$headers = 'From: noreply@avonloc.co.uk';
		$subject = "Forgotten password request ";
		$message = $user['fullName'].". We have just received a request for a new password for your user name ".$user['username'].". As a result we have changed your password to $password. Please use this next time you login to the avonLOC site. Thankyou.";
		if (isset($email))
		{
			mail($email,$subject,$message,$headers);
		}
		$this->log->trace("Email detail sent: $headers $subject $message");
		$this->smarty->assign("username",$user['username']);
		$this->smarty->display($this->FORGOTPASSWORD);
	}
	
	
	
	/****************************************************************************************************
	 * 					Specific functions
	 * **************************************************************************************************
	 */
	
	function DisplayHomePage()
	{
		//phpinfo();
		if (DEBUG) echo("in the home page");
		$this->log->debug("in the home page");
		$members=$this->dataLayer->getCommitteeMembers();
		if ( $members )
		{
			$members = array_slice($members, 0, 4);
		}
		
		$articleList=$this->dataLayer->getNews();
		if (isset($articleList))
		{
			$articleList = array_slice($articleList, 0, 1);
			if (is_array($articleList) || is_object($articleList))
			{
				foreach($articleList as $key => $article)
				{
					$phpdate = strtotime( $article['published'] );
					$articleList[$key]['published'] = date( 'd M Y', $phpdate );
				}
			}
		}
		$this->smarty->assign("articleList",$articleList);

		$courseList= $this->dataLayer->getCourses();
		if (isset ($courseList))
		{
			$courseList = array_slice($courseList, 0, 1);
		}
		$this->smarty->assign("courseList",$courseList);
		
		$this->smarty->assign("members",$members);
		$this->smarty->display ($this->HOME);
		//$this->ReadNews(2);
	}
	
	
	function PracticeInfo()
	{
		$this->smarty->display ($this->PRACTICEINFO);
	}
	
	function MaintainPractice()
	{
		$practiceData = $this->dataLayer->getPracticeData($_SESSION["userId"]);
		if (isset($practiceData))
		{
			$_SESSION["practiceId"]=$practiceData['practiceId'];
		}
		$this->smarty->assign("practice",$practiceData);
		$this->smarty->display ($this->POSTPRACTICE);
	}
	
	function SubmitPractice()
	{
		$this->refreshDataLayer('practice', 'practiceId');
		$this->DisplayHomePage();
	}
	
	function ViewPractices()
	{
		$practiceList = $this->dataLayer->getAllPracticeData();
		$this->smarty->assign("practiceList",$practiceList);
		$this->smarty->display ($this->VIEWPRACTICES);
	}
	
	function Vacancies()
	{
		$this->dataLayer->deleteOldVacancies();
		$vacancyList = $this->dataLayer->getAllVacancyData();
		if (is_array($vacancyList))
		{
			shuffle($vacancyList);
		}
		$this->smarty->assign("vacancyList",$vacancyList);
		$this->smarty->display ($this->VACANCIES);
	}
	

	
	function PostVacancies()
	{
		$this->smarty->display ($this->POSTVACANCIES);
	}
	
	function SubmitVacancies()
	{
		$this->refreshDataLayer('jobvacancies', 'vacancyId');
		$this->Vacancies();
	}
	
	function DeleteVacancy()
	{
		$this->refreshDataLayer('jobvacancies', 'vacancyId');
		$this->Vacancies();	
	}
	
	function Employees()
	{
		$employeeList = $this->dataLayer->getAllEmployeeData();
		$this->smarty->assign("employeeList",$employeeList);
		$this->smarty->display ($this->EMPLOYEES);
	}
	
	function OfferServices()
	{
		$employeeData = $this->dataLayer->getEmployeeData($_SESSION["userId"]);
		if(isset($employeeData))
		{
		if (count($employeeData)>0)
		{
			$_SESSION["employeeId"]=$employeeData['employeeId'];
		}
		}
		//$this->show($employeeData);
		//$this->show($_SESSION);
		$this->smarty->assign("employee",$employeeData);
		$this->smarty->display ($this->POSTSERVICES);
	}
	
	function SubmitServices()
	{
		$_REQUEST['mon'] = (isset($_POST['mon'])) ? 1 : 0;
		$_REQUEST['tue'] = (isset($_POST['tue'])) ? 1 : 0;
		$_REQUEST['wed'] = (isset($_POST['wed'])) ? 1 : 0;
		$_REQUEST['thu'] = (isset($_POST['thu'])) ? 1 : 0;
		$_REQUEST['fri'] = (isset($_POST['fri'])) ? 1 : 0;
		$_REQUEST['sat'] = (isset($_POST['sat'])) ? 1 : 0;
		$_REQUEST['sun'] = (isset($_POST['sun'])) ? 1 : 0;
		$this->refreshDataLayer('employee', 'employeeId');
		$this->Employees();
	}
	
	function DeleteEmployee()
	{
		$this->refreshDataLayer('employee', 'employeeId');
		$this->Employees();	
	}
	
	function About()
	{
		$members=$this->dataLayer->getCommitteeMembers();
		$this->smarty->assign("members",$members);
		$this->smarty->display ($this->ABOUT);
	}
	
	function Contact()
	{
	    // contact does not assume the user is registered any more Jan 2020.
		//$reader = $this->dataLayer->findItem('registereduser', $_SESSION['userId']);
		//$this->smarty->assign("reader",$reader);
		$this->smarty->display ($this->CONTACT);
	}

	function Subscribe()
    {
        $this->smarty->display($this->SUBSCRIBE);
    }
	
	/// set number to 0 to see all items
	function ReadNews($number)
	{
		$reader = $this->dataLayer->findItem('registereduser', $_SESSION['userId']);
		$articleList=$this->dataLayer->getNews();
		if ($number)
		{
			$articleList = array_slice($articleList, 0, $number);
		}
		if (is_array($articleList) || is_object($articleList))
		{
			foreach($articleList as $key => $article)
			{	
				$phpdate = strtotime( $article['published'] );
				$articleList[$key]['published'] = date( 'd M Y', $phpdate );
			}
		}
		$this->smarty->assign("articleList",$articleList);
		$this->smarty->assign("reader",$reader);
		$this->smarty->display ($this->READNEWS);
	}
	
	function SubmitNews()
	{
	    $captureStatus = $this->verifyCaptcha();
	    $this->log->debug($captureStatus);
	    if ($captureStatus != "success")
        {
            $this->postNews($captureStatus);
            return;
        }
//        // Validate reCAPTCHA box
//        if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])) {
//            // Google reCAPTCHA API secret key
//            $secretKey = RECAPTCHA_PRIVATE_KEY;
//
//            // Verify the reCAPTCHA response
//            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secretKey . '&response=' . $_POST['g-recaptcha-response']);
//
//            // Decode json data
//            $responseData = json_decode($verifyResponse);
//
//            // If reCAPTCHA response is valid
//            if (!$responseData->success) {
//                echo "<BR><BR><BR><BR>You are a bot! Go away!</p>";
//                die("fail...");
//            } else {
//                echo "<BR><BR><BR><BR>You are not not a bot!</p>";
//            }
//        }
//        else
//        {
//            echo "<BR><BR><BR><BR>You are a bot! Go away!</p>";
//            die("fail...");
//        }

		$inputName = "doc";
		if (isset($_FILES[$inputName]["name"]) && ($_FILES[$inputName]["name"] != "") )
		{
			$types=array("pdf","doc","docx","Pdf","Doc","Docx","PdF","DOC","DOCX");
			$isImage = false;
			$maxSizeBytes = 2000000;
			$maxSizeMessage = "Sorry, there is a limit of 2MB on news articles";
			$result =$this->upload($types,$isImage,$maxSizeBytes,$inputName,$maxSizeMessage);
			if ($result != false)
			{
				if ($result == "success")
				{
					$_REQUEST['fileLoaded']= 1;
					$_REQUEST['fileName']= $_FILES[$inputName]["name"];
				}
				else {
					$_REQUEST['fileLoaded']= 0;
					$this->log->debug(" doSubmitNews() failed to upload a doc with message ".$result);
					$this->smarty->assign("fileExists","true");
					$this->smarty->assign("headline",$_REQUEST['headline']);
					$this->smarty->assign("errorMessage",$result);
					$this->smarty->assign("newsDescription",$_REQUEST['newsDescription']);
					$this->smarty->assign("existingFileName",$_FILES[$inputName]["name"]);
					$this->PostNews();
					return;
				}
			}
		}
		$_REQUEST['published']= date ("Y-m-d",time());
		$this->log->trace("now is ".time()." in mysql form ".$_REQUEST['published']);
		//$_REQUEST['headline']=$this->dataLayer->mysqli->real_escape_string($_REQUEST['headline']);
		//$_REQUEST['newsDescription']=$this->dataLayer->mysqli->real_escape_string($_REQUEST['newsDescription']);
		
		$this->refreshDataLayer('news', 'newsId');	
		/// send a mail to subscribers.
		$subscriberList = $this->dataLayer->findItemsWithFilter('registereduser', "subscribedToNews=1", null, null);
		$headers = 'From: subscriptions@avonloc.co.uk';
		$subject = "New news article posted: ".$_REQUEST['headline'];
		$user=$this->dataLayer->findItem('registereduser', $_SESSION['userId']);
		$message = $user['fullName']." has just posted an article with the headline ".$_REQUEST['headline']." to read the article visit https:\\www.avonloc.co.uk, login and choose read news";
		if (is_array($subscriberList) || is_object($subscriberList))
		{
			foreach ($subscriberList as $subscriber)
			{
				mail($subscriber['email'],$subject,$message,$headers);
			}
		}
		$this->ReadNews(0);
	}
	
	function PostNews($status = "")
	{
        $this->log->trace($status);
	    $this->smarty->assign("RECAPTCHA_PUBLIC_KEY",RECAPTCHA_PUBLIC_KEY);
	    $this->smarty->assign("status",$status);
		$this->smarty->display ($this->POSTNEWS);
	}
	
	function ArchiveNews()
	{
		$this->refreshDataLayer('news', 'newsId');
		$this->ReadNews(null);	
	}
	function SubscribeNews()
	{
		$this->dataLayer->subscribeNews($_SESSION['userId'],1);
		$this->ReadNews(0);
	}
	function UnsubscribeNews()
	{
		$this->dataLayer->subscribeNews($_SESSION['userId'],0);
		$this->ReadNews(0);
	}	
	
	function AddCourse($status = "")
	{
        $this->smarty->assign("status",$status);
        $this->smarty->assign("RECAPTCHA_PUBLIC_KEY",RECAPTCHA_PUBLIC_KEY);
		$this->smarty->display ($this->ADDCOURSE);		
	}
	
	function ViewCourses()
	{
		//$courseList= $this->dataLayer->findItemsWithFilter(course, null, "courseName",false);
		$reader = $this->dataLayer->findItem('registereduser', $_SESSION['userId']);
		$courseList= $this->dataLayer->getCourses();
		if (is_array($courseList) || is_object($courseList))
		{
			foreach($courseList as $key => $course)
			{
				//$this->show($course);
				//$this->show($courseList[$key]);
			}
		}
		$this->smarty->assign("courseList",$courseList);
		$this->smarty->assign("reader",$reader);
		
		$this->smarty->display ($this->VIEWCOURSES);		
	}
	
	function SubmitCourse()
	{
        $captureStatus = $this->verifyCaptcha();
        $this->log->debug($captureStatus);
        if ($captureStatus != "success")
        {
            $this->addCourse($captureStatus);
            return;
        }
		$url = $_REQUEST['url'];
		if (strtolower(substr($url,0,3))== "www")
		{
			$_REQUEST['url'] = "http://".$url;
		}
		$this->refreshDataLayer('course', 'courseId');
		/// send a mail to subscribers.
		$subscriberList = $this->dataLayer->findItemsWithFilter('registereduser', "subscribedToCourses=1", null, null);
		$headers = 'From: subscriptions@avonloc.co.uk';
		$subject = "New CET course posted: ".$_REQUEST['courseName'];
		$user=$this->dataLayer->findItem('registereduser', $_SESSION['userId']);
		$message = $user['fullName']." has just posted a course called ".$_REQUEST['headline']." to see the details visit https:\\www.avonloc.co.uk, login and choose view courses";
		if (is_array($subscriberList) || is_object($subscriberList))
		{
		foreach ($subscriberList as $subscriber)
			{
				mail($subscriber['email'],$subject,$message,$headers);
			}
		}
		$this->ViewCourses();
	}
	
	function DeleteCourse()
	{
		$this->refreshDataLayer('course', 'courseId');
		$this->ViewCourses();		
	}
	
	function SubscribeCourses()
	{
		$this->dataLayer->subscribeCourses($_SESSION['userId'],1);
		$this->ViewCourses();
	}
	
	function UnsubscribeCourses()
	{
		$this->dataLayer->subscribeCourses($_SESSION['userId'],0);
		$this->ViewCourses();		
	} 
    
    function ManageDownloads($status = "")
    {
        $this->smarty->assign("status",$status);
//        $this->smarty->assign("RECAPTCHA_PUBLIC_KEY",RECAPTCHA_PUBLIC_KEY);
    	$downloadList= $this->dataLayer->findItemsWithFilter('download', null, "downloadTitle",false);
    	$this->smarty->assign("downloadList",$downloadList);
    	$this->smarty->display ($this->MANAGEDOWNLOADS);
    }
    
    function SubmitDownload()
    {
        //$captureStatus = $this->verifyCaptcha();
//        $this->log->debug($captureStatus);
//        if ($captureStatus != "success")
//        {
//            $this->ManageDownloads($captureStatus);
//            return;
//        }
    	$types=array("pdf","doc","docx","xls","xlsx","Pdf","Doc","Docx","Xls","Xlsx","PdF","DOC","DOCX","XLS","XLSX");
    	$isImage = false;
    	$maxSizeBytes = 2000000;
    	$inputName = "downloadDoc";
    	$maxSizeMessage = "Sorry, there is a limit of 2MB on documents to be uploaded";
    	$result =$this->upload($types,$isImage,$maxSizeBytes,$inputName,$maxSizeMessage);
    	if ($result != false)
    	{
    		if ($result == "success")
    		{
    			$_REQUEST['fileLoaded']= 1;
    			$_REQUEST['fileName']= $_FILES[$inputName]["name"];
    		}
    		else {
    			$_REQUEST['fileLoaded']= 0;
    			$this->log->debug(" doSubmitDownload() failed to upload a pdf with message ".$result);
    			$this->smarty->assign("fileExists","true");
    			$this->smarty->assign("downloadTitle",$_REQUEST['downloadTitle']);
    			$this->smarty->assign("errorMessage",$result);
    			$this->smarty->assign("existingFileName",$_FILES[$inputName]["name"]);
    			$this->ManageDownloads();
    			return;
    		}
    	}
    	$_REQUEST['published']= date ("Y-m-d",time());
    	$this->log->trace("now is ".time()." in mysql form ".$_REQUEST['published']);
    
    	$this->refreshDataLayer('download', 'downloadId');
    	$this->manageDownloads();
    }
    
    function DeleteDownload()
    {
//        $captureStatus = $this->verifyCaptcha();
//        $this->log->debug($captureStatus);
//        if ($captureStatus != "success")
//        {
//            $this->ManageDownloads($captureStatus);
//            return;
//        }
    	$this->deleteUploadDoc($_REQUEST['fileName']);
    	$this->refreshDataLayer('download', 'downloadId');
    	$this->ManageDownloads();
    }
    
    function ViewDownloads($title,$page)
    {
    	
    	$downloadList= $this->dataLayer->findItemsWithFilter('download', " pageToDisplay = '$page'", "downloadTitle",false);
    	$this->smarty->assign("title",$title);
    	$this->smarty->assign("downloadList",$downloadList);
    	$this->smarty->display ($this->VIEWDOWNLOADS);
    }
  
	
    function ManageLinks()
    {
    	$linkList= $this->dataLayer->findItemsWithFilter('link', null, "linkTitle",false);
    	$this->smarty->assign("linkList",$linkList);
    	$this->smarty->display ($this->MANAGELINKS);
    }
    
    function SubmitLink()
    {
    	$_REQUEST['published']= date ("Y-m-d",time());
    	$this->log->trace("now is ".time()." in mysql form ".$_REQUEST['published']);   
    	$url = $_REQUEST['url'];
		if (strtolower(substr($url,0,3))== "www")
    	{
    		$_REQUEST['url'] = "http://".$url;
    	}
    	$this->refreshDataLayer('link', 'linkId');
    	$this->ViewLinks();
    }
    
    
    function ViewLinks()
    {
    	$linkList= $this->dataLayer->findItemsWithFilter('link', null, "linkTitle",false);
    	$this->smarty->assign("linkList",$linkList);
    	$this->smarty->display ($this->VIEWLINKS);
    }
    
    function DeleteLink()
    {
    	$this->refreshDataLayer('link', 'linkId');
    	$this->ManageLinks();
    }
    
   function MaintainApplicants()
   {
    	$applicantList= $this->dataLayer->findItemsWithFilter('registereduser', "approval != 'rejected'", 'surname',null);
    	$this->smarty->assign("applicantList",$applicantList);
    	$this->smarty->display ($this->MAINTAINAPPLICANTS);
   }
   
   function MailingList()
   {
       $mailingList= $this->dataLayer->findItemsWithFilter('registereduser', "approval = 'approved'", 'surname',null);
       $uniqueList = [];/// The final array to use with no duplicates
       $dupFulArray = [];/// An array to just hold all emails including duplicates
       foreach ($mailingList as $person)
       {
           array_push($dupFulArray,$person['email']);
       }

       $oldEmails = $this->dataLayer->findItems('oldemail');
       foreach ($oldEmails as $person)
       {
           array_push($dupFulArray,$person['email']);
       }

       $uniqueList=array_unique($dupFulArray);
       $this->smarty->assign("mailingList",$uniqueList);
       $this->smarty->display ($this->MAILINGLIST);
   }
   
   function SubmitApplicant()
   {
   	/// cannot use generic as request variables clash with form variables.
	   	if ($_REQUEST["action"]=="update")
	   	{
	   		$this->dataLayer->approveApplicant($this->dataLayer->mysqli->real_escape_string($_REQUEST["userId"]));
	   		$newMember= $this->dataLayer->findItem('registereduser', $_REQUEST["userId"]);
	   		$emailAddress = $newMember['email'];
	   		$headers = 'From: noreply@avonloc.co.uk';
	   		$subject = "Welcome to the AvonLOC website: ";
	   		$message = "Welcome ".$_REQUEST['fullName'].". Your application to join the avonloc web site has now been approved. Visit https://www.avonloc.co.uk/avonloc.php?command=login&username=".$newMember['username'].", and you should now be able to login and enjoy the rest of the site. We hope you find it useful! Thanks, AvonLOC. ";  		
	   		mail($emailAddress,$subject,$message,$headers);	
	   		$this->log->trace("Have approved ".$_REQUEST['fullName']." and sent an email to $emailAddress with subject $subject and message $message");
	   	}
	   	else if ($_REQUEST["action"]=="delete")
	   	{
	   		$this->dataLayer->removeUser($this->dataLayer->mysqli->real_escape_string($_REQUEST["userId"]));
	   	}
	   	$this->MaintainApplicants();   		
   }
   
   function UploadPics()
   {
   		$user=$this->dataLayer->findItem('registereduser', $_SESSION['userId']);
    	$this->smarty->assign("user",$user);
   		$this->smarty->display ($this->UPLOADCOMMITTEEPICS);
   }
   
   function SubmitCommitteePics()
   {
   	$types=array("jpg","jpeg");
   	$isImage = true;
   	$maxSizeBytes = 500000;
   	$inputName = "downloadDoc";
   	$maxSizeMessage = "Sorry can you choose a smaller image. The limit is 500 KB. Larger files would slow down the home page for other users";
   	$result =$this->upload($types,$isImage,$maxSizeBytes,$inputName,$maxSizeMessage);
   	if ($result != false)
   	{
   		if ($result == "success")
   		{
   			$_REQUEST['fileLoaded']= 1;
   			$_REQUEST['picName']= $_FILES[$inputName]["name"];
   		}
   		else {
   			$_REQUEST['fileLoaded']= 0;
   			$this->log->debug(" doSubmitDownload() failed to upload an image with message ".$result);
   			$this->smarty->assign("fileExists","true");
   			$this->smarty->assign("errorMessage",$result);
   			$this->smarty->assign("existingFileName",$_FILES[$inputName]["name"]);
   			$this->UploadPics();
   			return;
   		}
   	}
   	$_REQUEST['published']= date ("Y-m-d",time());
   	$this->log->trace("now is ".time()." in mysql form ".$_REQUEST['published']);
   
   	$this->refreshDataLayer('registereduser','userId');
   	$this->UploadPics();
   }
    

	
	
	
}
