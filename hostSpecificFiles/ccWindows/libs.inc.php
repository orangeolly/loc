<?php
  # Filename: libs.inc.php

  # the exact path is defined.
  $fixpath = dirname(__FILE__);
  define  ("FIXPATH", dirname(__FILE__)); 

  $host = $_SERVER["HTTP_HOST"];
   $apachePath="D:\\Program Files (x86)\\Zend\\Apache2";
   $sitePath=$apachePath."\\htdocs\\avonloc";
   $extnsPath=$sitePath."\\extensionLibraries";
   if(!defined("SMARTY_DIR"))
   {
   	define ("SMARTY_DIR", $extnsPath."\\smarty-3.1.30\\libs\\");
   	define ("SMARTY_COMPILE_DIR", $apachePath."\\avonCompile\\");
   	//define ("PLUGINS_DIR", "D:\\Program Files (x86)\\Zend\\Apache2\\plugins\\");
   	//define ($apachePath."\\logs\\");
   	define ("LOCAL_CONFIG_PATH", $apachePath."\\conf\\");
   }
   date_default_timezone_set('Europe/London');
   error_reporting(E_ALL);
   if (DEBUG) echo (SMARTY_DIR."Smarty.class.php\n");
   require_once(SMARTY_DIR . "Smarty.class.php");
   if (DEBUG) echo ("Loaded smarty");
?>