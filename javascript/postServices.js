$(document).ready(function(){
	
	$("#tooBigWarning").hide();
	$("#vacancyDescription").keyup(function(){
		checkTextAreaLength($(this),800);
	});
	$("#vacancyDescription").click(function(){
		checkTextAreaLength($(this),800);
	});
	
	$("#email").keyup(function(){
		checkEmail();
	});	
	$("#email").click(function() {
		checkEmail();
	});
});