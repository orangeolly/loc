$(document).ready(function(){
	$( "input:checkbox" ).change(function(){
		$(".vacancyCardOuter").hide();
		if ($("#optometrist").prop('checked'))
		{
			$(".optometrist").show();
		}
		if ($("#dispensingOptician").prop('checked'))
		{
			$(".dispensingOptician").show();
		}
		if ($("#contactLensOptician").prop('checked'))
		{
			$(".contactLensOptician").show();
		}
	});
});