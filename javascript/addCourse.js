



function checkForm()
{
	$("#allFieldsWarning").html("Fields marked with a * must contain valid entries before you can post<br>");
	$("#allFieldsWarning").show();


    
    /// check fullname has a value
    var courseName = true;
    if ($("#courseName").val().length<1) courseName=false;
    
    ///check username is set
    var courseDate = true;
    if ($("#courseDate").val().length<1) courseDate=false;
    
    /// validate email
    var emailValid=validateEmail($("#email").val())
    
    /// decide whether to allow registration
	if( courseDate 
		&&
		emailValid
		&&
		courseName

	)
		{
		$("#saveForm").prop( "disabled", false ); 	
		$("#allFieldsWarning").hide();
		}
	else
		{
			$("#saveForm").prop( "disabled", true ); 
		}
}




/// Diplay if fullName is not long enough
function checkCourseName()
{
  if ($("#courseName").val().length==0) return;
  var courseName = true;
  if ($("#courseName").val().length<2) courseName=false;
  if (!courseName) $("#titlecourseName").addClass("warn");
  if (courseName) $("#titlecourseName").removeClass("warn");
}

function checkcourseDate()
{
  if ($("#courseDate").val().length==0) return;
  var courseDate = true;
  if ($("#courseDate").val().length<2) courseDate=false;
  if (!courseDate) $("#titlecourseDate").addClass("warn");
  if (courseDate) $("#titlecourseDate").removeClass("warn");
}


$(document).ready(function() 
{
	"use strict";
	$("#saveForm").prop( "disabled", true ); 
	$("#allFieldsWarning").hide();
	
	
	$("#email").keyup(function(){
		checkForm(); 
		checkEmail();
	});	
	$("#email").click(function() {
		checkForm();
		checkEmail();
	});


	$("#courseName").blur(function(){
		checkForm(); 
		checkCourseName();
	});
	$("#courseName").keyup(function(){
		checkForm(); 
		checkCourseName();
	});
	
	$("#courseDate").blur(function(){
		checkForm(); 
		checkcourseDate();
	});

	$("#courseDate").keyup(function(){
		checkForm(); 
		checkcourseDate();
	});

	
	
	$("#registerButtonDiv").mouseover(function(){
		checkForm();
		$("#allFieldsWarning").addClass("warn");
	});
	
});

