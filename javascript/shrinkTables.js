$(document).ready(function(){
	$('table tr').hide();
	//$('table > tr:first').show();
	$('table').each(function(){
		$(this).find('tr:first').show().addClass("topline");;
		$(this).click(function(){
			$(this).find('tr:first').toggle()
			$(this).find('tr').toggle();
			$(this).find('tr:first').show();
		});
		$(this).hover(function() {
		    $(this).css('cursor','pointer');
		}, function() {
		    $(this).css('cursor','auto');
		});

	});
})
