
$(document).ready(function(){
	
	$("#tooBigWarning").hide();
	$("#newsDescription").keyup(function(){
		checkTextAreaLength($(this),2000);
	});
	$("#newsDescription").click(function(){
		checkTextAreaLength($(this),2000);
	});
	$("#upload").click(function(){
		document.getElementById("fileToUpload").click();
	});
	
	$("#fileToUpload").change(function(){
		$("#uploadMessage").text("File selected is "+$(this)[0].files[0].name);
	});
	
	
});