ALTER TABLE registereduser
ADD COLUMN firstName VARCHAR(50) AFTER fullName;
ALTER TABLE registereduser
ADD COLUMN surname VARCHAR(50) AFTER firstName;