-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: db619665986.db.1and1.com
-- Generation Time: Jan 15, 2019 at 11:52 AM
-- Server version: 5.5.60-0+deb7u1-log
-- PHP Version: 7.0.33-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db619665986`
--
CREATE DATABASE IF NOT EXISTS `db619665986` DEFAULT CHARACTER SET latin1 COLLATE latin1_general_ci;
USE `db619665986`;

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `courseId` int(11) NOT NULL,
  `courseName` varchar(100) NOT NULL,
  `courseDate` varchar(100) DEFAULT NULL,
  `courseTime` varchar(100) DEFAULT NULL,
  `street` varchar(100) DEFAULT NULL,
  `ad1` varchar(50) DEFAULT NULL,
  `ad2` varchar(50) DEFAULT NULL,
  `ad3` varchar(50) DEFAULT NULL,
  `postCode` varchar(20) DEFAULT NULL,
  `coursePoints` varchar(100) DEFAULT NULL,
  `clo` varchar(3) DEFAULT NULL,
  `do` varchar(3) DEFAULT NULL,
  `courseEmail` varchar(100) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `courseAuthor` int(11) DEFAULT NULL,
  `courseShortDesc` varchar(500) DEFAULT NULL,
  `courseLongDesc` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `download`
--

CREATE TABLE `download` (
  `downloadId` int(11) NOT NULL,
  `downloadTitle` varchar(200) NOT NULL,
  `fileLoaded` tinyint(1) NOT NULL DEFAULT '0',
  `fileName` varchar(300) DEFAULT NULL COMMENT 'filename could include a long path.',
  `downloadAuthor` int(11) DEFAULT NULL,
  `lastModified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `published` date NOT NULL,
  `downloadDescription` varchar(2000) NOT NULL,
  `pageToDisplay` varchar(200) NOT NULL DEFAULT 'Download'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `employeeId` int(11) NOT NULL,
  `employeeTitle` varchar(100) NOT NULL DEFAULT '"missing"',
  `employeeDescription` varchar(1000) DEFAULT NULL,
  `postedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `userId` int(11) NOT NULL,
  `duration` int(11) NOT NULL DEFAULT '12',
  `status` varchar(20) NOT NULL DEFAULT '"pending"',
  `lastModified` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `employeeEmail` varchar(300) DEFAULT NULL,
  `employeePhone` varchar(300) DEFAULT NULL,
  `mon` tinyint(1) DEFAULT '0',
  `tue` tinyint(1) DEFAULT '0',
  `wed` tinyint(1) DEFAULT '0',
  `thu` tinyint(1) DEFAULT '0',
  `fri` tinyint(1) DEFAULT '0',
  `sat` tinyint(1) DEFAULT '0',
  `sun` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jobvacancies`
--

CREATE TABLE `jobvacancies` (
  `vacancyId` int(11) NOT NULL,
  `vacancyTitle` varchar(100) NOT NULL DEFAULT '"missing"',
  `vacancyDescription` varchar(1000) DEFAULT NULL,
  `postedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `vacancyAuthor` int(11) NOT NULL,
  `practiceId` int(11) DEFAULT NULL,
  `duration` int(11) NOT NULL DEFAULT '1',
  `status` varchar(20) NOT NULL DEFAULT '"pending"',
  `vacancyEmail` varchar(200) DEFAULT NULL,
  `optometrist` varchar(3) DEFAULT NULL,
  `dispensingOptician` varchar(3) DEFAULT NULL,
  `contactLensOptician` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `link`
--

CREATE TABLE `link` (
  `linkId` int(11) NOT NULL,
  `linkTitle` varchar(200) NOT NULL,
  `url` varchar(300) DEFAULT NULL COMMENT 'filename could include a long path.',
  `linkAuthor` int(11) DEFAULT NULL,
  `lastModified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `published` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `newsId` int(11) NOT NULL,
  `headline` varchar(200) NOT NULL,
  `newsDescription` varchar(2000) DEFAULT NULL,
  `fileLoaded` tinyint(1) NOT NULL DEFAULT '0',
  `imageLoaded` tinyint(1) NOT NULL DEFAULT '0',
  `fileName` varchar(300) DEFAULT NULL COMMENT 'filename could include a long path.',
  `published` date DEFAULT NULL,
  `newsAuthor` int(11) DEFAULT NULL,
  `newsStatus` varchar(20) NOT NULL DEFAULT 'live'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `practice`
--

CREATE TABLE `practice` (
  `practiceId` int(11) NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `practiceName` varchar(200) NOT NULL,
  `practiceEmail` varchar(200) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `street` varchar(50) DEFAULT NULL,
  `ad1` varchar(50) DEFAULT NULL,
  `ad2` varchar(50) DEFAULT NULL,
  `ad3` varchar(50) DEFAULT NULL,
  `postCode` varchar(10) DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `monToFri` varchar(50) DEFAULT NULL,
  `sun` varchar(50) DEFAULT NULL,
  `additionalInfo` varchar(500) DEFAULT NULL,
  `nhs` varchar(1) NOT NULL DEFAULT 'Y',
  `private` varchar(1) NOT NULL DEFAULT 'Y',
  `lastModified` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `practiceStatus` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `registereduser`
--

CREATE TABLE `registereduser` (
  `userId` int(11) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `hash` varchar(140) DEFAULT NULL,
  `lastLoginAttempt` int(11) DEFAULT NULL,
  `delay` float NOT NULL DEFAULT '0.25',
  `GOCNumber` varchar(12) DEFAULT NULL,
  `memberType` varchar(40) NOT NULL,
  `email` varchar(100) NOT NULL,
  `approval` varchar(20) NOT NULL DEFAULT 'pending',
  `fullName` varchar(200) DEFAULT NULL,
  `member` varchar(3) NOT NULL DEFAULT 'No',
  `role` varchar(30) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  `subscribedToNews` tinyint(1) DEFAULT '0',
  `subscribedToCourses` tinyint(1) DEFAULT '0',
  `fileLoaded` tinyint(1) DEFAULT NULL,
  `picName` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`courseId`);

--
-- Indexes for table `download`
--
ALTER TABLE `download`
  ADD PRIMARY KEY (`downloadId`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`employeeId`);

--
-- Indexes for table `jobvacancies`
--
ALTER TABLE `jobvacancies`
  ADD PRIMARY KEY (`vacancyId`);

--
-- Indexes for table `link`
--
ALTER TABLE `link`
  ADD PRIMARY KEY (`linkId`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`newsId`);

--
-- Indexes for table `practice`
--
ALTER TABLE `practice`
  ADD PRIMARY KEY (`practiceId`);

--
-- Indexes for table `registereduser`
--
ALTER TABLE `registereduser`
  ADD PRIMARY KEY (`userId`),
  ADD UNIQUE KEY `registeredUsercol_UNIQUE` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `courseId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `download`
--
ALTER TABLE `download`
  MODIFY `downloadId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `employeeId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jobvacancies`
--
ALTER TABLE `jobvacancies`
  MODIFY `vacancyId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `link`
--
ALTER TABLE `link`
  MODIFY `linkId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `newsId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `practice`
--
ALTER TABLE `practice`
  MODIFY `practiceId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `registereduser`
--
ALTER TABLE `registereduser`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
