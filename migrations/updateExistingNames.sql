UPDATE registereduser AS ru INNER JOIN
updated_names AS un
ON ru.userId = un.userId
SET ru.firstName = un.firstName, ru.surname = un.surname, ru.fullName = CONCAT(un.firstName," ",un.surname);