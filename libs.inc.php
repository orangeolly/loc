<?php
  # Filename: libs.inc.php

  # the exact path is defined.
  $fixpath = dirname(__FILE__);
  define  ("FIXPATH", dirname(__FILE__)); 

   if(!defined("SMARTY_DIR"))
   {
   			define ("SMARTY_DIR", FIXPATH."/extensionLibraries/smarty-3.1.30/libs/");
   			define ("SMARTY_COMPILE_DIR", FIXPATH."/compile/");
//   			define ("PLUGINS_DIR", "/linuxPath/plugins/");
   			define ("FLOG_PATH", FIXPATH."/avonloclog/");
   			define ("LOCAL_CONFIG_PATH", FIXPATH."/conf/");
   }
   date_default_timezone_set('Europe/London');
   require_once (SMARTY_DIR."Smarty.class.php");   
?>
