<link rel="stylesheet" type="text/css" href="./CSS/vacancy.css">
<!-- Modals -->
{foreach from=$articleList key=myId item=i}
<div id='{$i.newsId}' class="w3-modal"  style="overflow: hidden;">
    <div class="w3-modal-content w3-card-8 w3-animate-top">
      <header class="w3-container w3-light-blue"> 
        <span onclick="document.getElementById({$i.newsId}).style.display='none'" class="w3-closebtn">&times;</span>
        <h2>{$i.headline}</h2>
        <h4>Published {$i.published}</h4>

      </header>


        {$i.newsDescription}<BR>

        <footer class="w3-light-blue w3-container">
        {if $i.fileLoaded eq "1"}
        <h4>For more information ...</h4>
        <h4><a href="./uploads/{$i.fileName}" target="_blank"  style="overflow: hidden;">{$i.fileName}</a></h4>
        
        {else}
        <h4>&nbsp<br>

        </h4>
        {/if}
        </footer>
                </div>
</div>
{/foreach}
<!-- End of modals -->


<br class="bigOnly">
<br class="bigOnly">
<br class="bigOnly">
<div class="w3-container w3-padding-16">

 </div>

<!-- Basic news page -->
{foreach from=$articleList key=myId item=i}
<div class="w3-half w3-row-padding w3-margin-top ellipsis"  style="overflow: hidden;">
<div class="w3-card-2 _vacancyCard w3-white " onclick="document.getElementById('{$i.newsId}').style.display='block'">
<div >
        <div style="height:200px;padding:0 8px;overflow: hidden;cursor:pointer" onclick="document.getElementById('{$i.newsId}').style.display='block'">
        <h2>{$i.headline}</h2>
        <h4>Published on {$i.published}
        <span style="display:inline;font-size:20px;" id="newsAuthor">by AvonLOC. </span><br>
{if  ($sess_memberType == "admin") || $i.newsAuthor == $sess_userId}
		<form class="w3-center" style="font-size: 20px;display:inline;" method="post" action="./avonloc.php" enctype="multipart/form-data">
			<input type="hidden" name="command" value="archiveNews"> 
			<input type="hidden" name="action" value="update"> 
			<input type="hidden" name="newsStatus" value="archived"> 
			<input type="hidden" name="newsId" value="{$i.newsId}"> 
			Archive this article?
			<input title="Prevent this article for showing on this website?"   type="image" src="./images/delete_48.png" class="formSubmit"  width="15"><br>
		</form></h4>
{/if}		
        {$i.newsDescription}<BR>
        </div>
        <footer class="w3-light-blue w3-container" style="display:block">
        {if $i.fileLoaded eq "1"}
        <h4>For more information ...</h4>
        <h4><a href="./uploads/{$i.fileName}" target="_blank"  style="overflow: hidden;">{$i.fileName}</a></h4>
        
        {else}
        <h4>&nbsp<br>

        </h4>
        {/if}
        </footer>

</div>
</div>
</div>
{/foreach}

