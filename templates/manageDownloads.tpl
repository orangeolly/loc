<script src="javascript/generalForm.js" type="text/javascript"></script>
<script src="javascript/postNews.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="./CSS/vacancy.css">
<div id=main>
<br class="bigOnly">
<br class="bigOnly">
<br class="bigOnly">
<br class="bigOnly">

	<div class="w3-container w3-padding-64 w3-center">
	<h1>Upload document to be available for download</h1>
	<br> <br> <br>
		<form style="font-size: 25px;" method="post" action="./avonloc.php" enctype="multipart/form-data">
			<input type="hidden" name="command" value="submitDownload"> 
			<input type="hidden" name="action" value="create"> 
			<input type="hidden" name="downloadAuthor" value="{$sess_userId}">		
			 Document Title<br> <input class="headline"	type="text" name="downloadTitle" value="{$downloadTitle}"> <br> <br>
			  <div>
An optional short description<br>
			    <div>
			    <textarea title="A short description of the document"  name="downloadDescription" spellcheck="true"  rows ="10" maxlength="500">{$downloadDescription}</textarea>
			    </div>
			  </div>
			  			 			<br> <br>
				<span id="uploadMessage" >
{if $fileExists}
				{$errorMessage}
{else}
    			Upload the document in pdf, doc, docx, xls or xlxs format
{/if}	
				</span><br>
			<label id="upload" for="downloadDoc"  class="w3-btn w3-theme formSubmit">
    		<input type="file" name="downloadDoc" id="fileToUpload" style="display:none;" >
    		Select the pdf, doc, docx, xls or xlxs
    		</label>
    		<br> <br>
    		Choose which page this document should appear on
    		<select name="pageToDisplay">
    		<option>Download</option>
    		<option>Guideline</option>
    		<option>Banes</option>
    		<option>LOC</option>
    		<option>Enhanced services</option>
    		</select>
    		
    		 <br> <Br> 
			<input  id="postNewsSubmit" type="submit" class="w3-btn w3-theme formSubmit" value="Submit"><br>
			<br>
		</form>
	</div>
	

	<div class="w3-row-padding w3-margin-top w3-center"  style="font-size: 14pt;">
	<h2>Delete redundant downloads</h2>
<!-- Basic download page -->

{foreach from=$downloadList key=myId item=i}

		<form style="font-size: 25px;" method="post" action="./avonloc.php" enctype="multipart/form-data">
			<input type="hidden" name="command" value="deleteDownload"> 
			<input type="hidden" name="action" value="delete"> 
			<input type="hidden" name="downloadId" value="{$i.downloadId}"> 
			<input type="hidden" name="fileName" value="{$i.fileName}"> 
			<a href="./uploads/{$i.fileName}">{$i.pageToDisplay}'s doc {$i.downloadTitle}</a>
			<input title="delete {$i.downloadTitle} => {$i.fileName}"  id="postNewsSubmit"  type="image" src="./images/delete_48.png" class="formSubmit"  width="15"><br>

		</form>
{/foreach}
</div>
	
	
</div>