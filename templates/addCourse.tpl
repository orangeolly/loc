<script src="javascript/generalForm.js" type="text/javascript"></script>
<script src="javascript/addCourse.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="./CSS/register.css">
<div id=main>
<br class="bigOnly">
	<br class="bigOnly">
	<br class="bigOnly">
	<br class="bigOnly">


<form action="./avonloc.php" autocomplete="off" method="POST">
<input type="hidden" name="command" value="submitCourse">
<input type="hidden" name="action" value="create">
<input type="hidden" name="courseAuthor" value="{$sess_userId}">

  <header>
    <h2>Advertise Continuous Education and Training (CET)</h2>
  </header>
  
  <div>
    <label id="titlecourseName" class="desc"  for="courseName">Course name *</label>
    <div>
      <input title="The full title of the course" id="courseName" name="courseName" type="text" class="field text fn" value="{$courseName}" size="8"  spellcheck="true">
    </div>
  </div>
  <div>
    <label id="titlecourseShortDesc" class="desc"  for="courseShortDesc">Short Description</label>
    <div>
    <textarea title="A short description of the course" id="courseShortDesc" name="courseShortDesc" spellcheck="true"  cols="50" rows ="10" maxlength="500">{$courseShortDesc}</textarea>
    </div>
  </div>
  <div>
    <label id="titlecourseLongDesc" class="desc"  for="courseLongDesc">Long Description</label>
    <div>
    <textarea title="A long description of the course" id="courseLongDesc" name="courseLongDesc" spellcheck="true"  cols="50" rows="20" maxlength="2000">{$courseLongDesc}</textarea>
    </div>
  </div>


  <div>
    <label id="titlecourseDate" class="desc"  for="courseDate">Date(s) available *</label>
    <div>
      <input title="A clear description of the dates the training is available" id="courseDate" name="courseDate" type="text" class="field text fn" value="{$courseDate}" size="8"  spellcheck="true">
    </div>
  </div>
    
  <div>
    <label class="desc"  for="courseTime">Daily timings</label>
    <div>
      <input title="A clear description of the daily start and end times etc.." id="courseTime" name="courseTime" type="text" class="field text fn" value="{$courseTime}" size="8"  spellcheck="true">
    </div>
  </div>
    
<h2 class="w3-center">Course Location</h2>
			
			<div>
			<label for="street">Number and street</label>
			<div>
			<input	type="text" name="street" value="{$practice.street}">
			</div>
			</div>
			
			<div>
			<label for="practiceName">Address:</label>
			<div>
			<input type="text" name="ad1" id="ad1" value="{$practice.ad1}"/>
			<input type="text" name="ad2" id="ad2" value="{$practice.ad2}"/>
			<input type="text" name="ad3" id="ad3" value="{$practice.ad3}"/>
			</div>
			</div>
			
		
			<div>
			<label for="postCode">Post Code</label>
			<div>
			<input	type="text" name="postCode" value="{$practice.postCode}">
			</div>
			</div>    

 
		   <div>
			    <label class="desc"  for="coursePoints">Number of CET points</label>
			    <div>
			      <input  id="coursePoints" name="coursePoints" type="text" class="field text fn" value="{$coursePoints}" size="8"  spellcheck="false">
			    </div>
		   </div>
  
    		<div >
			    <label class="desc" for="clo">Applicable for CLO?</label>
			    <div>
			      <select id="clo" title=""  name="clo"  >
				      <option value="Yes">Yes</option>
				      <option value="No">No</option>
			      </select>
			    </div>
		    </div>
		    
		    <div >
			    <label class="desc" for="do">Applicable for DO?</label>
			    <div>
			      <select id="do" title=""  name="do"  >
				      <option value="Yes">Yes</option>
				      <option value="No">No</option>
			      </select>
			    </div>
		    </div>
			
		<div>
		    <label class="desc" id="titleEmail" for="courseEmail" >
		      Contact email *
		    </label>
		    <div>
		      <input id="email" name="courseEmail" type="email" spellcheck="false" maxlength="255" spellcheck="false"> 
		      <div id="emailMessage" class="warn"><br></div>
		   </div>
		  </div>
			
			<div>
			<label for="url">Web site</label>
			<div>
			<input	type="text" name="url" value="{$course.url}">
			</div>
			</div>  
			
  
  <div>
		<div id="registerButtonDiv">
  		<input id="saveForm" name="saveForm" type="submit" class="w3-btn w3-theme" value="Post Course">
  		<br><span id = "allFieldsWarning"></span>
    	</div>
	  <br>
	  <div class="text-xs-center captcha-block">
		  <div>{$status}</div>
		  <div class="g-recaptcha" data-sitekey="{$RECAPTCHA_PUBLIC_KEY}"></div>
	  </div>
	</div>
  
</form>
</div>
</body>


