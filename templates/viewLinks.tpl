<link rel="stylesheet" type="text/css" href="./CSS/vacancy.css_">
<br class="bigOnly">
<br class="bigOnly">
<br class="bigOnly">

<div class="w3-row-padding w3-margin-top w3-center" style="font-size: 14pt;" >
<h2>Useful links for AvonLOC members</h2>
<!-- Basic download page -->

{foreach from=$linkList key=myId item=i}

<div class="w3-container">
<a href="{$i.url}">{$i.linkTitle}</a><br>
{$i.linkDescription}
</div>
<br>
{/foreach}
</div>

