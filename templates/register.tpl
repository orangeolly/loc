<script src="javascript/generalForm.js" type="text/javascript"></script>
<script src="javascript/register.js" type="text/javascript"></script>
<script src="javascript/postNews.js" type="text/javascript"></script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<link rel="stylesheet" type="text/css" href="./CSS/register.css">
<div id=main>
<br class="bigOnly">
  <br class="bigOnly">
  <br class="bigOnly">
  <br class="bigOnly">

<form action="./avonloc.php" autocomplete="off" method="POST">
<input type="hidden" name="command" value="submitRegistrationRequest">

  <header>
    <h2>Apply for membership of the LOC site</h2>
  </header>
  
  <div>
    <label class="desc" id="titleFirstName" for="firstName">First name</label>
    <div>
      <input title="Your first name" id="firstName" name="firstName" type="text" class="field text fn" value="{$firstName}" size="8"  spellcheck="false" >
     </div>
     </div>
     <div>
      <label class="desc"  id="titleSurName" for="surname">Surname</label>    
      <div> 
      <input title="your surname" id="surname" name="surname" type="text" class="field text fn" value="{$surname}" size="8"  spellcheck="false"  >
    </div>
  </div>
    
  <div>
    <label class="desc" id="titleEmail" for="email" >
      Email
    </label>
    <div>
      <input id="email" name="email" type="email" spellcheck="false" value="{$email}" maxlength="255" spellcheck="false"> 
      <div id="emailMessage" class="warn"><br></div>
   </div>
  </div>







  <input  name="memberType" type="hidden" value="admin">
  <input  name="member" type="hidden" value="yes">
  <input  name="role" type="hidden" value="Member">





  <div id="GOCNum">
    <label class="desc" id="titleGOC" for="GOCNumber">GOC number</label>
    <div>
      <input title="" id="GOCNumber" name="GOCNumber" type="text" class="field text fn" value="{$GOCNumber}" size="8" >
    </div>
  </div>

    <div>
    <label class="desc" id="titleUsername" for="username">User name</label>
    <div>
      <input title="a unique username that to use to login to the site in future" id="username" name="username" type="text" class="field text fn form-control" 
      value="{$username}" 
      spellcheck="false"
      size="8" tabindex="9">
    </div>
  </div>
  
  <div>
    <label class="desc" id="titlePassword" for="password">Password <span id="toggleVis">(show)</span></label>
    <div>
      <input title="Min 8 characters, use length, Uppercase and numbers to strengthen" id="password" name="password" type="password" class="field text fn form-control" value="{$password}" size="8" tabindex="9"> 
      <div id="passMessage" class="warn"><br></div> 
    </div>
  </div>

  <div>
    <label class="desc" id="titlePasswordRpt" for="passwordRpt">Repeat password</label>
    <div>
      <input title="" id="passwordRpt" name="passwordRpt" type="password" class="field text fn" value="{$passwordRpt}" size="8" tabindex="9"> 
      <br><span id="repeatMessage"></span>   
    </div>
  </div>	  
  
  <div>
		<div id="registerButtonDiv">
  		<input id="saveForm" name="saveForm" type="submit" class="w3-btn w3-theme" value="register">
  		<br><span id = "allFieldsWarning">All fields must contain valid entries before you can register</span>
  		{if $usernameTaken eq "true"}
      <span id= "userNameTaken" ><br>Sorry the username {$userName} is taken please choose again</span>
      {/if}
  		
    </div>
    <br>
    <div class="text-xs-center captcha-block">
      <div>{$status}</div>
      <div class="g-recaptcha" data-sitekey="{$RECAPTCHA_PUBLIC_KEY}"></div>
    </div>

	</div>
  
</form>
</div>


