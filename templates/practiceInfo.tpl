<script src="javascript/shrinkTables.js" type="text/javascript"></script>  
<link rel="stylesheet" type="text/css" href="./CSS/practiceInfo.css" >   

<div id=practiceInfo>
<br class="bigOnly">
<br class="bigOnly">
<br class="bigOnly">
<h1 class="w3-center">Local Practice Information</h1>
								
    
  {foreach from=$practiceList key=myId item=i}                          
  <table width="10%" border="0" cellspacing="0" cellpadding="0" >

    <tr>
<!--       <td width="150" class="BodySubTitle">Practice Name:</td> -->
      <td class="BodySubTitle w3-center" colspan=2>{$i.practiceName}</td>
    </tr>
    
    <tr>
      <td><strong>Practice Address:</strong></td>
      <td>{$i.street}</td>
    </tr>
    
    <tr>
      <td>&nbsp;</td>
       <td>{$i.ad1}<br>{$i.ad2}<br>{$i.ad3}<br>{$i.postCode}</td>
    </tr>
    
    <tr>
      <td><strong>Practice Telephone:</strong></td>
      <td>{$i.telephone}</td>
    </tr>
    
    <tr>
      <td><strong>Practice Website:</strong></td>
      <td><a href="{$i.url}" target="_blank">{$i.url}</a></td>
    </tr>
    
    <tr>
      <td><strong>Practice Email:</strong></td>
      <td><a href="mailto:{$i.practiceEmail}" target="_top">{$i.practiceEmail}</a></td>
    </tr>
    
    <tr>
      <td valign="top"><strong>Opening Hours: </strong></td>
      <td><strong>Mon -> Sat:</strong>{$i.monToFri}</td>
    </tr>
    
    <tr>
      <td valign="top">&nbsp;</td>
      <td><strong>Sun & Bank hols:</strong>{$i.sun}</td>
    </tr>
    

    
    <tr>
      <td valign="top">&nbsp;</td>
      <td><strong>Additional info: </strong>{$i.additionalInfo}</td>
    </tr>
    
    <tr>
      <td><strong>NHS:</strong></td>
      <td>{$i.nhs}</td>
    </tr>
    
    <tr>
      <td><strong>Private:</strong></td>
      <td>{$i.private}</td>
    </tr>
    {if ($i.userId == $sess_userId) || $sess_memberType == "admin"}
    <tr>
      <td class="w3-center" colspan=2 title="Delete this practice from the website?"><strong>Delete this practice from the website:
		<form class="w3-center" style="font-size: 15px;display:inline;" method="post" action="./avonloc.php" enctype="multipart/form-data">
			<input type="hidden" name="command" value="submitPractice"> 
			<input type="hidden" name="action" value="delete"> 
			<input type="hidden" name="practiceId" value="{$i.practiceId}"> 
			<input title="delete this practice from the website?"   type="image" src="./images/delete_48.png" class="formSubmit"  width="15"><br>
		</form>      
      </strong></td>
    </tr>
    {/if}

  </table>
 {/foreach} 
</div>