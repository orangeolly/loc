<link rel="stylesheet" type="text/css" href="./CSS/about.css">
<div class="w3-container w3-padding-64 " style="margin: 0px auto;font-size:17px;max-width:1050px;">
<br class="bigOnly">
<br class="bigOnly">
<br class="bigOnly">

<!--h1>ABOUT THE LOC</h1>
<p>Avon LOC consists of up to 12 members. Elections for vacancies on the LOC take place at the Annual General Meeting, and a term of office is 3 years.

<p>Click on the link below to see
<br><a href="./uploads/constitution.doc">The LOC Constitution</a>

<h2>Members are listed below, along with their LOC e-mail addresses and a brief description of their roles in the LOC.</h2-->
<h1>Officers:</h1>

<b>Chair: Amar Shah</b> - An Optometrist and franchise practice owner, Specialist Hospital Optometrist and LOCSU Optical Lead. The point of contact for any queries as well as the main lead on any commissioning work for the LOC<br>
EMail:<a href="mailto:chair@avonloc.co.uk">chair@avonloc.co.uk</a><br>

<b>Interim Vice Chair: Andrew Edwards</b> - An Independent Optometrist and practice owner. Our lead for the Bath and North East Somerset area and queries associated to this patch.<br>
EMail:<a href="mailto:bath@avonloc.co.uk">bath@avonloc.co.uk</a><br>

<b>Treasurer: Andrew Pinn</b> - An Independent Optometrist and practice owner. Our financial lead for the LOC and also our Cataract lead for community. <br>
EMail:<a href="mailto:treasurer@avonloc.co.uk">treasurer@avonloc.co.uk</a><br>

<b>Secretary: Amy Hughes</b> -A Locum Optometrist, Phd Student and Clinical Governance Performance Lead for Primary Eyecare Services. The central point of contact for general enquiries, new members and holder of our mailing list. <br>
EMail:<a href="mailto:secretary@avonloc.co.uk">secretary@avonloc.co.uk</a><br>

<h1>Committee Members:</h1>

<b>Lynne Fernandes:</b> Independent Optometrist and practice owner. Avon LOC representative for NHS England PLDP (performers list decision panel) South, South West; and referral guidelines lead.
<br><b>Mark Humphrey-Ali:</b> Independent Dispensing Optician and practice owner. Lead for DO’s and Low vision services.
<br><b>John Hopcroft:</b> Multiple Optometrist and professional services manager. Lead for MECs.
<br><b>Meera Patel:</b> Multiple Optometrist and practice Director. Co lead for BaNES
<br><b>Jenny Odigi:</b> Independent Optometrist and practice owner. Sponsorship lead
<br><b>Mona Thacker:</b> Locum Optometrist. Community engagement Lead
<br><b>Peter Turner:</b> Independent Optometrist and practice owner; Specialist Hospital Optometrist. Previous vice chair and Glaucoma lead, now supporting committee where needed.
<br><b>Gareth Whatley:</b> Lecturer at UWE, Multiple resident Optometrist. Social media lead.



