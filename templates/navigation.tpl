<script src="javascript/navigation.js?v=2" type="text/javascript"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">

<link rel="stylesheet" type="text/css" href="./CSS/navigation.css">
<div id="outer">
	<div id = "top" class="w3-top" >
	<ul id="underlinedList" class="w3-navbar w3-card-2 w3-black w3-large">
	  <li><a href="./avonloc.php" title="home" class="w3-light-blue"><i class="fa fa-home w3-large"></i></a></li>
	     {*if $sess_approval eq "approved"*}
	  <li class="w3-dropdown-hover">
		    <a href="javascript:void(0)">News <i class="fa fa-caret-down"></i></a>
	    <div class="w3-dropdown-content w3-white w3-card-4">
	      <a  href="./avonloc.php?command=readNews">Read news</a>
            {if  $sess_memberType == "admin"}
	      <a  href="./avonloc.php?command=postNews">Add an article</a>
            {/if}
	    </div>
	  </li>
	  <li class="w3-dropdown-hover">
		    <a href="javascript:void(0)">Education <i class="fa fa-caret-down"></i></a>
	    <div class="w3-dropdown-content w3-white w3-card-4">
            {if  $sess_memberType == "admin"}
	      <a  href="./avonloc.php?command=addCourse">Add a course</a>
            {/if}
	      <a  href="./avonloc.php?command=viewCourses">View courses</a>
	    </div>
	  </li>  
	  <li class="w3-dropdown-hover">
		    <a href="javascript:void(0)">Reference info <i class="fa fa-caret-down"></i></a>
	    <div class="w3-dropdown-content w3-white w3-card-4">
	      <a  href="https://remedy.bnssgccg.nhs.uk/adults/ophthalmology/referral-urgency-guidance/">Referral Guidelines</a>
	      <a  href="./avonloc.php?command=viewDownloads">Downloads</a>
	      <a  href="./avonloc.php?command=viewBanes">BANES</a>
	      <a  href="./avonloc.php?command=viewLOC">LOC</a>
	      <a  href="./avonloc.php?command=viewEnhancedServices">Enhanced services</a>
	      <a  href="./avonloc.php?command=viewLinks">Useful links</a>
	      {if  $sess_memberType == "admin"}
	      <a  href="./avonloc.php?command=manageDownloads">Maintain site content</a>
	      <a  href="./avonloc.php?command=mailingList">Show current mailing list</a>
	      <a  href="./avonloc.php?command=manageApplications">Maintain applications</a>
	      {/if}
	      {if ($sess_rank)  || ($sess_memberType == "admin")}
	      <!--a  href="./avonloc.php?command=committeePics">Upload committee pics</a -->
	      {/if}
	    </div>
	  </li>
		<li><a href="http://eepurl.com/gvCEaT" title="SubscribeToMailingList" class="">Subscribe to Mail List</a></li>
        {if  $sess_memberType == "admin"}
			<li><a href="./avonloc.php?command=register" title="Register" >Register</a></li>
        {/if}
<!-- 	  </ul> -->
<!-- 	  <ul class="w3-right"> -->
<!-- 	  <ul class="w3-right"> -->
   {if $sess_approval != "pending"}  
      <!--li><a id="registerLink" href="./avonloc.php?command=register">Register</a>	</li-->
    {/if}     
	  <li class="w3-right"><a id="aboutLink" href="./avonloc.php?command=about">About</a></li>
		<li class="w3-right"><a id="aboutLink" href="./avonloc.php?command=contact">Contact</a></li>
		<!--li class="w3-right"><a id="subscribelink" href="./avonloc.php?command=subscribe">Subscribe</a></li-->
		<!-- move login to another page alltogether-->
	  <!--li class="w3-right"><a id="loginLink" href="./avonloc.php?command=login">Login</a></li-->


  {if $sess_approval eq "approved"}
  <li class="w3-right"><a id="logLink" href="./avonloc.php?command=logout">Logout</a></li> 
 {/if}
	</ul>
	</div>

	
	
