<script src="javascript/generalForm.js" type="text/javascript"></script>
<script src="javascript/postNews.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="./CSS/vacancy.css">
<div id=main>
<br class="bigOnly">
<br class="bigOnly">
<br class="bigOnly">
<br class="bigOnly">

	<div class="w3-container w3-padding-64 w3-center">
	<h1>Supply a useful link for the site</h1>
	<h4>Please ensure that all aspects of the site you are linking to are appropriate and that the link is available to everyone.</h4>
	<h4>Avoid linking to documents that are only available to those logged in to a site.</h4>
	<h4>Please make it your responsibility to delete the link if it is redundant or broken.</h4>
	<br> <br> <br>
		<form style="font-size: 25px;" method="post" action="./avonloc.php" enctype="multipart/form-data">
			<input type="hidden" name="command" value="submitLink"> 
			<input type="hidden" name="action" value="create"> 
			<input type="hidden" name="linkAuthor" value="{$sess_userId}">		
			 Please provide and informative name for the link<br> 
			 <input class="headline" type="text" name="linkTitle"> <br> <br>
				Supply the web address here<br>
				<input class="headline" type="text" name="url"> <br> <br>
							  <div>
An optional short description<br>
			    <div>
			    <textarea title="A short description of the document"  name="linkDescription" spellcheck="true"  rows ="10" maxlength="500">{$linkDescription}</textarea>
			    </div>
			  </div>
			  			 			<br> <br>
    		
    		 <br> <Br> 
			<input  id="postNewsSubmit" type="submit" class="w3-btn w3-theme formSubmit" value="Post"><br>
			<br>
		</form>
	</div>
	<div class="w3-row-padding w3-margin-top w3-center"  style="font-size: 14pt;">
	<h2>Delete redundant links</h2>
<!-- Basic download page -->

{foreach from=$linkList key=myId item=i}

		<form style="font-size: 25px;" method="post" action="./avonloc.php" enctype="multipart/form-data">
			<input type="hidden" name="command" value="deleteLink"> 
			<input type="hidden" name="action" value="delete"> 
			<input type="hidden" name="linkId" value="{$i.linkId}"> 
			<a href="{$i.url}">{$i.linkTitle}</a>
			<input title="delete {$i.linkTitle} => {$i.url}"  id="postNewsSubmit"  type="image" src="./images/delete_48.png" class="formSubmit"  width="15"><br>
		</form>
{/foreach}
</div>		
</div>