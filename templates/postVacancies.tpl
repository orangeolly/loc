<script src="javascript/generalForm.js" type="text/javascript"></script>
<script src="javascript/postVacancies.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="./CSS/vacancy.css">
<div id=main>
<br class="bigOnly">
<br class="bigOnly">
<br class="bigOnly">

	<div class="w3-container w3-padding-64 w3-center">
	<h1>You can advertise jobs for your practice here</h1>
	<br> <br> <br>
		<form style="font-size: 25px;" method="post" action="./avonloc.php">
			<input type="hidden" name="command" value="submitVacancy"> 
			<input type="hidden" name="action" value="create"> 
			<input type="hidden" name="vacancyAuthor" value="{$sess_userId}">
			
			 Job Title<br> <input
				type="text" name="vacancyTitle"> <br>
		  <div>
		    <label class="desc" id="titleEmail" for="vacancyEmail" >
		      Contact email
		    </label>
		    <div>
		      <input id="email" name="vacancyEmail" type="email" spellcheck="false" value="" maxlength="255" spellcheck="false"> 
		      <div id="emailMessage" class="warn"><br></div>
		   </div>
		  </div>			
				 <br>
			Job Description <i>(include contact numbers if necessary)</i><BR> <textarea id="vacancyDescription"
				name="vacancyDescription"></textarea><br> <br> <Br> 
			<input  id="postVacancySubmit" type="submit" class="w3-btn w3-theme formSubmit" value="Post vacancy"><br>
			<span id="tooBigWarning"> You have reached the 800 Character limit for vacancies </span>
			<br>
			Vacancy should be available for <i>select relevant below</i><br><br class="smallOnly">
			<input type="checkbox" name="optometrist" value="yes"> &nbsp Optometrists<br><br class="smallOnly">
			<input type="checkbox" name="dispensingOptician" value="yes"> &nbsp Dispensing Opticians<br><br class="smallOnly">
			<input type="checkbox" name="contactLensOptician" value="yes">&nbsp  Contact Lens Opticians<br><br class="smallOnly">

			<label for="duration">How long should the advert run for? <i>(months)</i></label>
			<select name="duration" id="duration" >
			<option>1</option>
			<option>2</option>
			<option>3</option>
			</select>
		</form>
	</div>
	<br> <br> <br> <br> <br> <br> <br> <br>
	<br> <br> <br>
</div>