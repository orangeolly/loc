<script src="javascript/vacancy.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="./CSS/vacancy.css">

<!-- Modals -->
{foreach from=$vacancyList key=myId item=i}
<div id='{$i.vacancyId}' class="w3-modal" style="overflow: hidden;">
    <div class="w3-modal-content w3-card-8 w3-animate-top">
      <header class="w3-container w3-light-blue"> 
        <span onclick="document.getElementById({$i.vacancyId}).style.display='none'" class="w3-closebtn">&times;</span>
        <h4>{$i.vacancyTitle}</h4>

      </header>
      <div class="w3-container">
        <p>{$i.vacancyDescription}</p>
      </div>
      {if $i.practiceName neq ""}
      <div class="w3-container w3-padding-32">    
      <div><strong>At {$i.practiceName}</strong></div>
      <strong>Address:</strong>
      <div>{$i.street}</div>
      <div>{$i.ad1}</div>
      <div>{$i.ad3}</div>
      <div>{$i.postCode}</div>
      <div><strong>Telephone:</strong></div>
      <div>{$i.telephone}</div>
      <div><a href="{$i.url}" target="_blank">{$i.url}</a></div>
      <div><a href="mailto:{$i.practiceEmail}">{$i.practiceEmail}</a></div>
      <div><strong>NHS:</strong>{$i.nhs}</div>
      <div><strong>Private:</strong>{$i.private}</div>
      </div>
      {/if}
      <div class="w3-container w3-padding-32">
       Job open to     {if $i.optometrist} Optometrists {/if}{if $i.dispensingOptician} Dispensing opticians {/if} {if $i.contactLensOptician} Contact lens opticians {/if}
      </div>

      <footer class="w3-container w3-light-blue">
        <p>To apply contact {$i.fullName}{if ($i.vacancyEmail)} at <a href="mailto:{$i.vacancyEmail}">{$i.vacancyEmail}</a>{/if}</p>
      </footer>
    </div>
</div>
{/foreach}
<!-- End of modals -->

<div id=main  class="w3-light-grey"  style="overflow: hidden;">
<div id="vacancyMain"  >
<br class="bigOnly">
<br class="bigOnly">
<br class="bigOnly">
<br class="bigOnly">
<br class="bigOnly">
<div style="padding-left:32px;">
	Show vacancies for <i>(select relevant) </i>&nbsp
	<br class="smallOnly">
	<br class="smallOnly">
	<input  type="checkbox" id="optometrist" name="optometrist" value="yes" checked>&nbsp &nbsp Optometrists &nbsp<br class="smallOnly"><br class="smallOnly">
	<input   type="checkbox" id="dispensingOptician" name="dispensingOptician" value="yes" checked>&nbsp &nbsp  Dispensing Opticians &nbsp<br class="smallOnly"><br class="smallOnly">
	<input   type="checkbox" id="contactLensOptician" name="contactLensOptician" value="yes" checked>&nbsp &nbsp  Contact Lens Opticians <br class="smallOnly">
</div>
<!-- Basic job ads -->
<div class="w3-row-padding w3-margin-top"  style="overflow: hidden;">
{foreach from=$vacancyList key=myId item=i}

<div class=" vacancyCardOuter w3-third_ {if $i.optometrist}optometrist{/if}{if $i.dispensingOptician}dispensingOptician{/if} {if $i.contactLensOptician}contactLensOptician{/if}">
<div class="w3-card-8 w3-margin"  >
<div class="w3-container vacancyCard  w3-white"  onclick="document.getElementById('{$i.vacancyId}').style.display='block'">
<h2 class="w3-title vacancyTitle"  >{$i.vacancyTitle}</h2>
  <p>{$i.vacancyDescription}
</div>

<footer class="w3-container" style="height:80px;">
<strong>Open to:</strong> {if $i.optometrist} Optometrists {/if}{if $i.dispensingOptician} Dispensing opticians {/if} {if $i.contactLensOptician} Contact lens opticians {/if}  
{if  ($sess_memberType == "admin") || $i.vacancyAuthor == $sess_userId}
<div id="deleteVacancy">
		<form class="w3-center" style="font-size: 15px;padding:16px;" method="post" action="./avonloc.php" enctype="multipart/form-data">
			<input type="hidden" name="command" value="deleteVacancy"> 
			<input type="hidden" name="action" value="delete"> 
			<input type="hidden" name="vacancyId" value={$i.vacancyId}> 
			Delete this vacancy?
			<input title="delete this vacancy?"  type="image" src="./images/delete_48.png" class="formSubmit"  width="15"><br>
		</form>
</div>
{else}
<br><br><br>
{/if}

</footer>

</div>
</div>
{/foreach}
<div class="w3-card-4">


</div>
</div>
</div>
</div>
