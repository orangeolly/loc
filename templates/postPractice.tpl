<script src="javascript/generalForm.js" type="text/javascript"></script>
<script src="javascript/postServices.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="./CSS/register.css">
<!-- <link rel="stylesheet" type="text/css" href="./CSS/postServices.css"> -->
<link rel="stylesheet" type="text/css" href="./CSS/postPractice.css">
<div id=main>
<br class="bigOnly">
<br class="bigOnly">
<br class="bigOnly">

	<div class="w3-container ">
	
				<h1 class="w3-center">Please provide your practice details for the site</h1>
				<br>
				
		<form style="font-size: 25px;" method="post" action="./avonloc.php">
			<input type="hidden" name="command" value="submitPractice"> 
			<input type="hidden" name="action" value="createOrUpdate"> 
			<div>
			<label for="practiceName">Practice Name </label>
			<div>
			<input title="if multiple, include location"	type="text" name="practiceName" value="{$practice.practiceName}">
			<br><i><span style="font-size:12px">if multiple, include location</span></i>
			</div>
			</div>
			
			<div>
		    <label class="desc" id="titleEmail" for="practiceEmail" >
		      Practice email
		    </label>
		    <div>
		      <input id="email" name="practiceEmail" type="email" spellcheck="false" value="{$practice.practiceEmail}" maxlength="255" spellcheck="false"> 
		      <div id="emailMessage" class="warn"><br></div>
		   </div>
		  </div>
			
			<div>
			<label for="url">Web site</label>
			<div>
			<input	type="text" name="url" value="{$practice.url}">
			</div>
			</div>
	
						<h2 class="w3-center">Practice Address</h2>
			
			<div>
			<label for="street">Number and street</label>
			<div>
			<input	type="text" name="street" value="{$practice.street}">
			</div>
			</div>
			
			<div>
			<label for="practiceName">Address:</label>
			<div>
			<input type="text" name="ad1" id="ad1" value="{$practice.ad1}"/>
			<input type="text" name="ad2" id="ad2" value="{$practice.ad2}"/>
			<input type="text" name="ad3" id="ad3" value="{$practice.ad3}"/>
			</div>
			</div>
			
		
			<div>
			<label for="postCode">Post Code</label>
			<div>
			<input	type="text" name="postCode" value="{$practice.postCode}">
			</div>
			</div>
			
			<div>
			<label for="telephone">Telephone number</label>
			<div>
			<input	type="text" name="telephone" value="{$practice.telephone}">
			</div>
			</div>
			<h2 class="w3-center">Opening hours</h2>
			<div>
			<label for="monToFri">Monday to Saturday</label>
			<div>
			<input	type="text" name="monToFri" value="{$practice.monToFri}">
			</div>
			</div>
			
			
			<div>
			<label for="sun">Sunday and bank holidays</label>
			<div>
			<input	type="text" name="sun" value="{$practice.sun}">
			</div>
			</div>
			
			<div>
			<label for="additionalInfo">Additional opening info.</label>
			<div>
			<textarea name="additionalInfo" >{$practice.additionalInfo}</textarea>
			</div>
			</div>
			
			<div>
			<label for="nhs">NHS</label>
			<div>
			<select name="nhs"><option>{$practice.nhs}</option><option>Y</option><option>N</option></select>
			</div>
			</div>
			
			<div>
			<label for="private">Private</label>
			<div>
			<select name="private"><option>{$practice.private}</option><option>Y</option><option>N</option></select>
			</div>
			</div>
			
			<div  class="w3-center">
			<input   type="submit" class="w3-btn w3-theme formSubmit w3-center" value="Post this information">
			</div>
		</form>


		<form style="font-size: 25px;" method="post" action="./avonloc.php">
			<input type="hidden" name="command" value="submitPractice"> 
			<input type="hidden" name="action" value="delete"> 			<br>
			<div  class="w3-center">
			<input   type="submit" class="w3-btn w3-theme formSubmit" value="Remove"><br>
			</div>
		</form>


	</div>
</div>