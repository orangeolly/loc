<script src="javascript/generalForm.js" type="text/javascript"></script>
<script src="javascript/postNews.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="./CSS/vacancy.css">
<div id=main>
<br class="bigOnly">
<br class="bigOnly">
<br class="bigOnly">
<br class="bigOnly">

	<div class="w3-container w3-padding-64 w3-center">
	<img alt="No picture yet" src="./uploads/{$user.picName}" style="width:150px;height:150px;">
	<h1>Upload a picture to be shown on the homepage or about page</h1>
	<h2>We have applied a limit of 500KB on picture size, so you will probably have to save your picture in a small format first</h2>
	<h2>This is because your picture may be shown on the home page, and a big picture would therefore slow the website for viewers with a low bandwidth connection</h2>
	<br> <br> <br>
		<form style="font-size: 25px;" method="post" action="./avonloc.php" enctype="multipart/form-data">
			<input type="hidden" name="command" value="submitCommitteePic"> 
			<input type="hidden" name="action" value="update"> 
			<input type="hidden" name="downloadAuthor" value="{$sess_userId}">		
			<input type="hidden" name="userId" value="{$sess_userId}">		
			
				<span id="uploadMessage" >
{if $fileExists}
				{$errorMessage}
{else}
    			Upload the picture
{/if}	
				</span><br>
			<label id="upload" for="downloadDoc"  class="w3-btn w3-theme formSubmit">
    		<input type="file" name="downloadDoc" id="fileToUpload" style="display:none;" >
    		Select the image
    		</label>   		
    		 <br> <Br> 
			<input  id="postNewsSubmit" type="submit" class="w3-btn w3-theme formSubmit" value="Submit"><br>
			<br>
		</form>
	</div>
	
	
</div>