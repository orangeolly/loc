<script src="javascript/generalForm.js" type="text/javascript"></script>
<script src="javascript/postNews.js" type="text/javascript"></script>


<link rel="stylesheet" type="text/css" href="./CSS/vacancy.css">

<div id=main>
<br class="bigOnly">
<br class="bigOnly">
<br class="bigOnly">

	<div class="w3-container w3-padding-64 w3-center">
	<h1>Post news items here</h1>
	<br> <br> <br>
		<form style="font-size: 25px;" method="post" action="./avonloc.php" enctype="multipart/form-data">
			<input type="hidden" name="command" value="submitNews"> 
			<input type="hidden" name="action" value="create"> 
			<input type="hidden" name="newsAuthor" value="{$sess_userId}">		
			 Headline<br> <input class="headline"
				type="text" name="headline" value={$headline}> <br> <br>
			Description starting with a summary paragraph<BR> <textarea id="vacancyDescription"
				name="newsDescription">{$newsDescription}</textarea><br>
				<span id="uploadMessage" >
{if $fileExists}
				{$errorMessage}
{else}
    			Upload a pdf, doc or docx with the full article if needed
{/if}				
				</span><br>
			<label id="upload" for="doc"  class="w3-btn w3-theme formSubmit">
    		<input type="file" name="doc" id="fileToUpload" style="display:none;" >
    		Select the pdf, doc or docx
    		</label>
    		
    		 <br> <Br> 
			<input  id="postNewsSubmit" type="submit" class="w3-btn w3-theme formSubmit" value="Post this article"><br>
			<span id="tooBigWarning"> You have reached the 2000 Character limit for news articles </span>
			<br>
			<div class="text-xs-center captcha-block">
				<div>{$status}</div>
				<div class="g-recaptcha" data-sitekey="{$RECAPTCHA_PUBLIC_KEY}"></div>
			</div>
			<br/>


	</div>
	<br> <br> <br> <br> <br> <br> <br> <br>
	<br> <br> <br>
</div>