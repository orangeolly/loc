<link rel="stylesheet" type="text/css" href="./CSS/about.css">
<div class="w3-container w3-padding-64 w3-center" style="margin: 0px auto;font-size:25px;max-width:1000px;">
<br class="bigOnly">
<br class="bigOnly">
<br class="bigOnly">

<h1>Contact Avon LOC</h1>

<p>
If you would like to receive information and updates from Avon LOC please register for mail shots
<a href="http://eepurl.com/gvCEaT">here</a>.
<br>
This will subscribe you to our external mailing list informing you of any key news or information. This will also be available on the website.
<p>
If you have any queries you can contact us through our chair: 
<a href="mailto:chair@avonloc.co.uk">chair@avonloc.co.uk</a>
        or secretary: <a href="mailto:secretary@avonloc.co.uk">secretary@avonloc.co.uk</a>.
</P>


</div>
