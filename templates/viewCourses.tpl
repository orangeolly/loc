<script src="javascript/course.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="./CSS/course.css">
<!-- <link rel="stylesheet" type="text/css" href="./CSS/practiceInfo.css" >  -->
<!-- Modals -->
{foreach from=$courseList key=myId item=i}

<div id='{$i.courseId}' class="w3-modal">
    <div class="w3-modal-content w3-card-8 w3-animate-top">
      <header class="w3-container w3-light-blue"> 
        <span onclick="document.getElementById({$i.courseId}).style.display='none'" class="w3-closebtn">&times;</span>
        <h4>{$i.courseName}</h4>

      </header>
      <div class="w3-container">
      {$i.courseLongDesc}<br>
      <br>
      CLO:{$i.clo}
      &nbspDO:{$i.do}
      &nbspOO:Yes<br>
      points:{$i.coursePoints}<br><br>
Date(s) available {$i.courseDate}<br><br>
Daily timing {$i.courseTime}<br><br>
<table class="w3-white" style="min-width:100px;max-width: 300px;margin: 0px">
      <td style="width:100px;"><strong>Address:</strong></td>
      <td>{$i.street}</td>
    </tr>
    
    <tr>
      <td>&nbsp;</td>
       <td>{$i.ad1}<br>{$i.ad2}<br>{$i.ad3}<br>{$i.postCode}</td>
    </tr>
</table>
{if ($i.url)}
Website:<a href="{$i.url}">{$i.url}</a>
{/if}
      </div>
<div id="deleteCourse">
		<form style="font-size: 15px;padding:16px;" method="post" action="./avonloc.php" enctype="multipart/form-data">
			<input type="hidden" name="command" value="deleteCourse"> 
			<input type="hidden" name="action" value="delete"> 
			<input type="hidden" name="courseId" value="{$i.courseId}"> 
			This course was posted by {$i.fullName}<br>
			{if  ($sess_memberType == "admin") || $i.courseAuthor == $sess_userId}
			Delete this course?
			<input title="delete this course" type="image" src="./images/delete_48.png" class="formSubmit"  width="15"><br>
			{else}
			<br>
			{/if}
		</form>
</div>
      
      <footer class="w3-container w3-light-blue">
        <p>To apply contact {$i.fullName} at <a href="mailto:{$i.courseEmail}">{$i.courseEmail}</a></p>
      </footer>
    </div>
</div>

{/foreach}
<!-- End of modals -->

<div id=main>
<div id="courseMain" class="w3-white">
<br class="bigOnly">
<br class="bigOnly">
<br class="bigOnly">



<!-- Basic job ads -->

<div class=" w3-center ">

 <h2>The following courses are currently available</h2>
 <h3><i>Click on a course to see more detail</i></h3>
<table style="max-width:600px;border:2px;" >

<tr><th>Course name</th><th title="Contact lens opticians">CLO</th><th title="Dispensing opticians">DO</th><th title="Optometrists">OO</th><th>Points</th></tr>
{foreach from=$courseList key=myId item=i}
<tr  onclick="document.getElementById('{$i.courseId}').style.display='block'" style="cursor:pointer;">
  <td><span style="cursor:pointer;" class="w3-title courseTitle" ><a href="javascript:void(0)">{$i.courseName}</a></span></td>
  <td title="Contact lens opticians">{$i.clo}</td>
  <td title="Dispensing opticians">{$i.do}</td>
  <td title="Optometrists">Yes</td>
  <td>{$i.coursePoints}</td>
  </tr>
  <tr onclick="document.getElementById('{$i.courseId}').style.display='block'" style="cursor:pointer;"><td colspan="5">{$i.courseShortDesc}</td></tr>
  <tr><td colspan="5">&nbsp</td></tr>
{/foreach}
</table>
</div>

</div>
</div>
