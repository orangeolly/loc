<script src="javascript/generalForm.js" type="text/javascript"></script>
<script src="javascript/postServices.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="./CSS/postServices.css">
<div id=main>
<br class="bigOnly">
<br class="bigOnly">
<br class="bigOnly">

	<div class="w3-container w3-padding-64 w3-center">
		<h1>You can advertise that you are available for work here</h1>
		<div class="formTitle w3-margin-32">{$sess_fullName}</div>
		<form style="font-size: 25px;" method="post" action="./avonloc.php">
			<input type="hidden" name="command" value="submitServices"> 
			<input type="hidden" name="action" value="createOrUpdate"> 
			 Summary<br> <input	type="text" name="employeeTitle" value="{$employee.employeeTitle}"> <br> <br>
			Work required, area's available to work, contact numbers etc...<BR> <textarea id="employeeDescription"
				name="employeeDescription" >{$employee.employeeDescription}</textarea><br>
							<div>
		    <label class="desc" id="titleEmail" for="employeeEmail" >
		      Contact email
		    </label>
		    <div>
		      <input id="email" name="employeeEmail" type="email"  value="{$employee.email}" maxlength="255" spellcheck="false"> 
		      <div id="emailMessage" class="warn"><br></div>
		   </div>
		   
		    <label class="desc" id="titlePhone" for="employeePhone" >
		      Contact numbers
		    </label>
		    <div>
		      <input id="phone" name="employeePhone" type="text" value="{$employee.employeePhone}" maxlength="100" spellcheck="false"> 
		   </div>
		   Which days are you available?<br>
		    Mon	<input type="checkbox" name="mon" {if ($employee.mon)} checked{/if} value=1>
		   	Tue <input type="checkbox" name="tue" {if ($employee.tue)} checked{/if} value=1>
		   	Wed <input type="checkbox" name="wed"  {if ($employee.wed)} checked{/if} value=1><br class="smallOnly">  
		   	Thu <input type="checkbox" name="thu"  {if ($employee.thu)} checked{/if} value=1>
		   	Fri <input type="checkbox" name="fri"  {if ($employee.fri)} checked{/if} value=1> 
		   	Sat <input type="checkbox" name="sat"  {if ($employee.sat)} checked{/if} value=1><br class="smallOnly"> 
		   	Sun <input type="checkbox" name="sun"  {if ($employee.sun)} checked{/if} value=1> 

		  </div>
				 <br> <Br> 
			<input   type="submit" class="w3-btn w3-theme formSubmit" value="Post this advert"><br>
		</form>

		<form style="font-size: 25px;" method="post" action="./avonloc.php">
			<input type="hidden" name="command" value="submitServices"> 
			<input type="hidden" name="action" value="delete"> 			<br>
			<input   type="submit" class="w3-btn w3-theme formSubmit" value="Remove"><br>
		</form>
		<span id="tooBigWarning">You have reached the 800 Character limit</span>
		<br>
		<div>This will be removed automatically one year after the latest modification</div>
	</div>
	<br> <br> <br> <br> <br> <br> <br> <br>
	<br> <br> <br>
</div>