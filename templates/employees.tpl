
<script src="javascript/employee.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="./CSS/vacancy.css">
<link rel="stylesheet" type="text/css" href="./CSS/employees.css">
<!-- Modals -->
{foreach from=$employeeList key=myId item=i}
<div id='{$i.employeeId}' class="w3-modal">
    <div class="w3-modal-content w3-card-8 w3-animate-top">
      <header class="w3-container w3-light-blue"> 
        <span onclick="document.getElementById({$i.employeeId}).style.display='none'" class="w3-closebtn">&times;</span>
        <h4>{$i.employeeTitle}</h4>

      </header>
      <div class="w3-container">
        <p>{$i.employeeDescription}</p>
      </div>
      <footer class="w3-container w3-light-blue">
        <p>Contact {$i.fullName}
        {if ($i.employeeEmail)}
         at <a href="mailto:{$i.employeeEmail}">{$i.employeeEmail}</a>
         {/if}
         </p>
      </footer>
    </div>
</div>
{/foreach}
<!-- End of modals -->

<div id=main class="w3-light-grey">
<div id="vacancyMain" >
<br class="bigOnly">
<br class="bigOnly">
<br class="bigOnly">
<br class="bigOnly">
<br class="bigOnly">

<!-- Basic job ads -->

<div style="padding-left:32px;">
	Show vacancies for <i>(select relevant) </i>&nbsp
	<br>
	<br class="smallOnly">
	<input  type="checkbox" id="mon" name="mon"  checked>  &nbsp Mon &nbsp &nbsp &nbsp
	<input  type="checkbox" id="tue" name="tue"  checked>  &nbsp Tue &nbsp &nbsp &nbsp
	<input  type="checkbox" id="wed" name="wed"  checked>  &nbsp Wed &nbsp &nbsp<br class="smallOnly"><br class="smallOnly">
	<input  type="checkbox" id="thu" name="thu"  checked> &nbsp Thu &nbsp &nbsp &nbsp
	<input  type="checkbox" id="fri" name="fri"  checked>  &nbsp Fri &nbsp &nbsp &nbsp
	<input  type="checkbox" id="sat" name="sat"  checked> &nbsp Sat &nbsp &nbsp<br class="smallOnly"><br class="smallOnly">
	<input  type="checkbox" id="sun" name="sun"  checked>  &nbsp Sun &nbsp &nbsp
</div>

<div class="w3-row-padding w3-margin-top">

{foreach from=$employeeList key=myId item=i}
<div class=" vacancyCardOuter {if $i.mon} mon {/if}{if $i.tue} tue {/if} {if $i.wed} wed {/if} {if $i.thu} thu {/if} {if $i.fri} fri {/if} {if $i.sat} sat {/if} {if $i.sun} sun {/if}">
<div class="w3-card-8 w3-margin"  >
<div class="w3-container vacancyCard  w3-white"  onclick="document.getElementById('{$i.employeeId}').style.display='block'">
<h2 class="w3-title employeeTitle"  >{$i.employeeTitle}</h2>
  <p>{$i.employeeDescription}
</div>

<footer class="w3-container" style="height:80px;">
<strong>Available on:</strong> {if $i.mon} Mon {/if}{if $i.tue} Tue {/if} {if $i.wed} Wed {/if} {if $i.thu} Thu {/if} {if $i.fri} Fri {/if} {if $i.sat} Sat {/if} {if $i.sun} Sun {/if}  
<div id="deleteEmployee">
		<form class="w3-center" style="font-size: 15px;padding:16px;" method="post" action="./avonloc.php" enctype="multipart/form-data">
			<input type="hidden" name="command" value="deleteEmployee"> 
			<input type="hidden" name="action" value="delete"> 
			<input type="hidden" name="employeeId" value="{$i.employeeId}"> 
			{if  ($sess_memberType == "admin") || $i.userId == $sess_userId}
			Delete this advert?
			<input title="delete this advert from the website?"   type="image" src="./images/delete_48.png" class="formSubmit"  width="15"><br>
			{else}
			<br>
			{/if}
		</form>
</div>
</footer>
</div>
</div>
{/foreach}

</div>
</div>
</div>
