<link rel="stylesheet" type="text/css" href="./CSS/practiceInfo.css" >   
<br class="bigOnly">
<br class="bigOnly">
<br class="bigOnly">

<div class="w3-container w3-padding-64 w3-center" style="margin: 0px auto;font-size:18px;max-width:1000px;">
<h2>The following applications are pending approval</h2>
{if !($applicantList)}
None at the moment
{/if}
<table>
{foreach from=$applicantList key=myId item=i}
{if $i.approval eq "pending" and $i.memberType eq "admin"}
<tr>
<th style="text-align:center;" colspan="2">{$i.fullName}</th>
</tr>
<tr>
<td>Type</td><td >{$i.memberType}</td>
</tr>
<tr>
<td>Email</td>
<td > <a href="mailto:{$i.email}">{$i.email}</a></td>
</tr>
<tr>
<td>GOC number</td>
<td >{$i.GOCNumber}</td>
</tr>
<tr>
<td>Approval?</td>
<td >{$i.approval}</td>
</tr>
<tr>
<td style="text-align:center" colspan="2">
	<form style="font-size: 18px;" method="post" action="./avonloc.php" enctype="multipart/form-data">
		<input type="hidden" name="command" value="submitApplicant"> 
		<input type="hidden" name="action" value="update"> 
		<input type="hidden" name="userId" value="{$i.userId}"> 
		<input type="hidden" name="approval" value="approved"> 
		<input title="Approve {$i.fullName} for this role" type="submit" class="w3-btn w3-theme formSubmit w3-center" value="Approve?" style="font-size:18px;">		
	</form>or 
	<form style="font-size: 18px;" method="post" action="./avonloc.php" enctype="multipart/form-data">
		<input type="hidden" name="command" value="submitApplicant"> 
		<input type="hidden" name="action" value="delete"> 
		<input type="hidden" name="userId" value="{$i.userId}"> 
		<input title="delete {$i.fullName}" type="submit" class="w3-btn w3-theme formSubmit w3-center" value="Remove?" style="font-size:18px;">
	</form>	
</td>
</tr>

{/if}
{/foreach}
</table>
------------------
<br>
<br>
<h2>A full list of site users with approval level.</h2>
<table>

{foreach from=$applicantList key=myId item=i}

{if $i.memberType eq "admin"}
<tr>
<th style="text-align:center;" colspan="2">{$i.fullName}</th>
</tr>
<tr>
<td>Type</td><td >{$i.memberType}</td>
</tr>
<tr>
<td>Email</td>
<td > <a href="mailto:{$i.email}">{$i.email}</a></td>
</tr>
<tr>
<td>GOC number</td>
<td >{$i.GOCNumber}</td>
</tr>
<tr>
<td>Approval?</td>
<td >{$i.approval}</td>
</tr>
<tr>
<td style="text-align:center;" colspan="2">
	<form style="font-size: 18px;" method="post" action="./avonloc.php" enctype="multipart/form-data">
		<input type="hidden" name="command" value="submitApplicant"> 
		<input type="hidden" name="action" value="delete"> 
		<input type="hidden" name="userId" value="{$i.userId}"> 
		<input title="delete {$i.fullName}" type="submit" class="w3-btn w3-theme formSubmit w3-center" value="Remove?" style="font-size:18px;">
	</form>	
</td>
</tr>
{/if}
{/foreach}
</table>
</div>


